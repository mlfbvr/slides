const nrwlConfig = require('@nrwl/react/plugins/webpack.js');

module.exports = (config) => {
  nrwlConfig(config);

  config.module.rules.push({
    test: /(\.raw(\.[^.]+)?|\.example)$/i,
    use: [{ loader: 'raw-loader' }],
  });

  return config;
};
