module.exports = {
  projects: [
    '<rootDir>/apps/continuous-l10n',
    '<rootDir>/libs/deck-components',
    '<rootDir>/libs/prism',
  ],
};
