import { spectacleThemeMlfbvr } from './spectacle-theme-mlfbvr';

describe('spectacleThemeMlfbvr', () => {
  it('should work', () => {
    expect(spectacleThemeMlfbvr()).toEqual('spectacle-theme-mlfbvr');
  });
});
