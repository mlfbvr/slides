import createTheme from 'spectacle/lib/themes/default';

import '@fontsource/montserrat/100.css';
import '@fontsource/montserrat/100-italic.css';
import '@fontsource/montserrat/200.css';
import '@fontsource/montserrat/200-italic.css';
import '@fontsource/montserrat/300.css';
import '@fontsource/montserrat/300-italic.css';
import '@fontsource/montserrat/400.css';
import '@fontsource/montserrat/400-italic.css';
import '@fontsource/montserrat/500.css';
import '@fontsource/montserrat/500-italic.css';
import '@fontsource/montserrat/600.css';
import '@fontsource/montserrat/600-italic.css';
import '@fontsource/montserrat/700.css';
import '@fontsource/montserrat/700-italic.css';
import '@fontsource/montserrat/800.css';
import '@fontsource/montserrat/800-italic.css';
import '@fontsource/montserrat/900.css';
import '@fontsource/montserrat/900-italic.css';

import '@fontsource/space-grotesk/300.css';
import '@fontsource/space-grotesk/400.css';
import '@fontsource/space-grotesk/500.css';
import '@fontsource/space-grotesk/600.css';
import '@fontsource/space-grotesk/700.css';

import '@fontsource/fira-code/variable.css';
import '@fontsource/fira-code';

export const mlfbvrTheme = createTheme(
  {
    primary: 'white',
    secondary: '#1F2022',
    tertiary: '#03A9FC',
    quaternary: '#ffd700',
  },
  {
    primary: 'Space Grotesk, Montserrat',
    secondary: 'Space Grotest, Montserrat',
  }
);
