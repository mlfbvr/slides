import components from 'spectacle/node_modules/prismjs/components.js';
import getLoader from 'spectacle/node_modules/prismjs/dependencies';

const loadedLanguages = new Set();

const silent = false;

export function loadLanguages(Prism, languages) {
  if (languages === undefined) {
    languages = Object.keys(components.languages).filter((l) => l !== 'meta');
  } else if (!Array.isArray(languages)) {
    languages = [languages];
  }

  // the user might have loaded languages via some other way or used `prism.js` which already includes some
  // we don't need to validate the ids because `getLoader` will ignore invalid ones
  const loaded = [...loadedLanguages, ...Object.keys(Prism.languages)];

  getLoader(components, languages, loaded).load((lang) => {
    if (!(lang in components.languages)) {
      if (!silent) {
        console.warn('Language does not exist: ' + lang);
      }
      return;
    }

    delete Prism.languages[lang];

    require(`spectacle/node_modules/prismjs/components/prism-${lang}`);

    loadedLanguages.add(lang);
  });
}
