import Prism from 'spectacle/node_modules/prismjs/components/prism-core';
import 'prism-themes/themes/prism-a11y-dark.css';
import { loadLanguages } from './load-languages';

export const bootstrapPrism = (languages = []) => {
  loadLanguages(Prism, [
    ...languages,
    // Looks like spectacle needs this one too
    'handlebars',
  ]);

  Prism.highlightAll();
};
