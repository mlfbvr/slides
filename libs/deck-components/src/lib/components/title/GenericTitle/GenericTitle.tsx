import React from 'react';
import { Heading } from 'spectacle';
import classNames from './title.module.scss';

export default ({ title }) => (
  <Heading caps lineHeight={1} textColor="white" className={classNames.title}>
    {title}
  </Heading>
);
