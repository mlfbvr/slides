import React from 'react';
import { Code, Table, TableBody, TableItem, TableRow } from 'spectacle';

import classNames from './table.module.scss';

export default ({ pluralRules }) => (
  <Table className={classNames.table}>
    <TableBody>
      {pluralRules.map(({ keyword, rule, preformatted }) => (
        <TableRow key={keyword}>
          <TableItem className={classNames.keyword}>{keyword}</TableItem>
          <TableItem>{preformatted ? rule : <Code>{rule}</Code>}</TableItem>
        </TableRow>
      ))}
    </TableBody>
  </Table>
);
