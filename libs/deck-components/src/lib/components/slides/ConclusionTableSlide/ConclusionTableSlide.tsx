import React from 'react';
import {
  Heading,
  Slide,
  Table,
  TableBody,
  TableItem,
  TableRow,
} from 'spectacle';
import cn from 'classnames';

import classNames from './table.module.scss';

const DIFFICULTY_TO_SYMBOL = {
  easy: '👍', // 'nice',
  medium: '😐', // 'meh',
  hard: '👎', // 'hell',
};

const DIFF_RANK = {
  easy: 3,
  medium: 2,
  hard: 1,
};

const trend = (label, difficulty, previous) => {
  const previousDifficulty = previous.find(
    (c) => c.label === label
  )?.difficulty;

  if (previousDifficulty) {
    const previousRank = DIFF_RANK[previousDifficulty];
    const rank = DIFF_RANK[difficulty];

    if (previousRank < rank) {
      return 'up';
    }
    if (previousRank > rank) {
      return 'down';
    }
    return '';
  }
};

export default ({
  hash = undefined,
  title,
  criterias,
  previous = [],
  showPrevious = false,
  children = null,
}) => (
  <Slide hash={hash} className={classNames.slide}>
    <Heading className={classNames.title} size={2}>
      {title}
    </Heading>

    <Table className={classNames.table}>
      <TableBody>
        {criterias.map(({ label, note, difficulty }) => (
          <TableRow key={label}>
            <TableItem>{label}</TableItem>
            {showPrevious &&
              (() => {
                const previousDifficulty = previous.find(
                  (c) => c.label === label
                )?.difficulty;

                return (
                  <TableItem
                    className={cn(
                      classNames.difficulty,
                      classNames[previousDifficulty]
                    )}
                  >
                    {DIFFICULTY_TO_SYMBOL[previousDifficulty]}
                  </TableItem>
                );
              })()}
            <TableItem
              className={cn(classNames.difficulty, classNames[difficulty])}
            >
              <span data-trend={trend(label, difficulty, previous)}>
                {DIFFICULTY_TO_SYMBOL[difficulty]}
              </span>
            </TableItem>
            <TableItem className={cn(classNames.note)}>{note}</TableItem>
          </TableRow>
        ))}
      </TableBody>
    </Table>
    {children}
  </Slide>
);
