export { default as TitleSlide } from './TitleSlide/TitleSlide';
export { default as ConclusionTableSlide } from './ConclusionTableSlide/ConclusionTableSlide';
export { default as GenericSlide } from './GenericSlide/GenericSlide';
