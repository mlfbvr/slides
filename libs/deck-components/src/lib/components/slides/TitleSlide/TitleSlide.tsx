import React from 'react';
import { Slide } from 'spectacle';

import GenericTitle from '../../title/GenericTitle/GenericTitle';

export default ({ hash = undefined, title, children = null }) => (
  <Slide
    transition={['fade']}
    hash={hash}
    textColor="white"
    contentStyles={{
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      width: '100%',
      maxWidth: '100%',
      height: '100%',
      maxHeight: '100%',
      padding: '0 10vw',
      background:
        'linear-gradient(to bottom, #171717 20%, transparent 150%), linear-gradient(to right, #00d4ff, #020024)',
    }}
  >
    <GenericTitle title={title} />
    {children}
  </Slide>
);
