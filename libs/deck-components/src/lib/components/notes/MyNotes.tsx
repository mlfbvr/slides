import React from 'react';
import { Notes } from 'spectacle';

export class MyNotes extends React.Component {
  // static contextTypes = {
  //   store: PropTypes.object,
  // };
  //
  // static childContextTypes = {
  //   slideHash: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  // };
  //
  // getChildContext() {
  //   console.log(this.context.store.getState());
  //   return {
  //     slideHash: this.context.slideIndex,
  //   };
  // }

  render() {
    return <Notes>{this.props.children}</Notes>;
  }
}
