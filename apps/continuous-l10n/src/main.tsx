import React from 'react';
import * as ReactDOM from 'react-dom';

import 'normalize.css/normalize.css';
import './styles.scss';

import bootstrap from './bootstrap';
import './bootstrap/prism';

const start = () =>
  import('./app/App').then(({ default: App }) =>
    ReactDOM.render(<App />, document.getElementById('root'))
  );

bootstrap().then(start);
