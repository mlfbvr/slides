import React from 'react';
import { ConclusionTableSlide, MyNotes } from '@mlfbvr/deck-components';
import { DX } from '../../constants';

export const ConclusionDX = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <ConclusionTableSlide
      title="DX - First vs Continuous"
      criterias={DX.CONTINUOUS}
      previous={DX.NAIVE}
      showPrevious
      hash={slideIndex}
    >
      <MyNotes>
        <p>
          In terms of DX we had a little tradeoff: the solution isn't that
          simple to implement anymore. But we gained a lot in terms of
          scalability thanks to gettext, capacity to handle complex messages
          with the ICU syntax, and to integrate the translations from the
          translators with weblate.
        </p>
        <p>
          Little bonus: the translations still follow versioning, since they are
          pushed back in our codebase through pull requests, which allows us to
          rollback if we had a major default in production without losing
          translations in the process.
        </p>
      </MyNotes>
    </ConclusionTableSlide>
  )
);
