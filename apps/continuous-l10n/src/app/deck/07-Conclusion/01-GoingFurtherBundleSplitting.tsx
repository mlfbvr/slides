import React from 'react';
import CodeSlide from 'spectacle-code-slide';

export const GoingFurtherBundleSplitting = React.memo(
  ({ slideIndex }: { slideIndex?: number }) => (
    <CodeSlide
      slideIndex={slideIndex}
      transition={[]}
      lang="jsx"
      code={
        /* eslint-disable-next-line @typescript-eslint/no-var-requires */
        require('./01-GoingFurtherBundleSplitting/code.raw').default
      }
      ranges={[
        {
          loc: [5, 9],
          title: 'messages in state',
          notes:
            'we can bundle split the translations files. First, we add the messages in the state of our locale provider.',
        },
        {
          loc: [15, 18],
          title: 'Load langs on-the-fly',
          notes:
            'we then use the import function to load the messages on the fly',
        },
        {
          loc: [32, 36],
          title: 'Pass messages',
          notes:
            'and we pass the messages from the state to the react intl provider.',
        },
      ]}
    />
  )
);
