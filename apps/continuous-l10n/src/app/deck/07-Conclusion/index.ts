import { GoingFurtherTitle } from './00-GoingFurtherTitle';
import { GoingFurtherBundleSplitting } from './01-GoingFurtherBundleSplitting';
import { GoingFutherBrowserSnippets } from './02-GoingFutherBrowserSnippets';
import { Questions } from './04-Questions';
import { ConclusionDX } from './ConclusionDX';
import { ConclusionTX } from './ConclusionTX';
import { DXTXConclusion } from './DXTXConclusion';
import { TheEnd } from './05-TheEnd';

export default [
  GoingFurtherTitle,
  GoingFurtherBundleSplitting,
  GoingFutherBrowserSnippets,
  DXTXConclusion,
  ConclusionDX,
  ConclusionTX,
  // Conclusion,
  Questions,
  TheEnd,
];
