function setHtmlLangAttribute(locale) {
  const htmlEl = document.documentElement;

  if (htmlEl) {
    htmlEl.setAttribute('lang', locale);
  }
}
