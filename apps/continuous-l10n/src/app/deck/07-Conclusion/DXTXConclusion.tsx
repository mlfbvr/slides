import React from 'react';
import { MyNotes, TitleSlide } from '@mlfbvr/deck-components';

export const DXTXConclusion = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <TitleSlide title="Solutions comparison" hash={slideIndex}>
      <MyNotes>
        <p>As a conclusion, let's compare our first and last solutions.</p>
      </MyNotes>
    </TitleSlide>
  )
);
