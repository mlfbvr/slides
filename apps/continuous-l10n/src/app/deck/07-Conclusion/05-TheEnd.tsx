import React from 'react';
import { Heading, Slide, Text } from 'spectacle';
import classNames from '../00-Intro/Intro.module.scss';
import sfeirLogo from '../../../assets/logo-sfeir.svg';
import { Links } from '../Links';

export const TheEnd = React.memo(({ slideIndex }: { slideIndex: number }) => (
  <Slide
    hash={slideIndex}
    transition={['zoom']}
    contentStyles={{
      width: '100%',
      maxWidth: '100%',
      height: '100%',
      maxHeight: '100%',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-between',
      alignItems: 'flex-start',
      padding: '4rem',
      color: 'white',
      background:
        'linear-gradient(to bottom, #171717 20%, transparent 150%), linear-gradient(to right, #00d4ff, #020024)',
    }}
  >
    <header className={classNames.header}>
      <img style={{ height: '2rem' }} src={sfeirLogo} alt="logo sfeir" />
    </header>
    <main className={classNames.main}>
      <Text margin="10px 0 0" textColor="white" bold italic>
        It was "Continuous Localization"
      </Text>
      <Text margin="10px 0 0" textColor="white" bold italic>
        Thank you for listening.
      </Text>
      <Heading
        size={1}
        caps
        lineHeight={1}
        textColor="white"
        className={classNames.title}
      >
        THE END
      </Heading>
    </main>
    <footer className={classNames.footer}>
      <Links
        href="https://openfeedback.io/vbWb8fXavC7gLlQzjpAx/2022-06-21/hyie5Z5CsXY9xpKstdjF"
        label="Give your feedback!"
      />
      <Text margin="10px 0 0" textColor="white" bold>
        Maxence LEFEBVRE
      </Text>
    </footer>
  </Slide>
));
