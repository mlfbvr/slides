import React from 'react';
import { ConclusionTableSlide, MyNotes } from '@mlfbvr/deck-components';
import { TX } from '../../constants';

export const ConclusionTX = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <ConclusionTableSlide
      hash={slideIndex}
      title="TX - First vs Continuous"
      criterias={TX.CONTINUOUS}
      previous={TX.NAIVE}
      showPrevious
    >
      <MyNotes>
        <p>
          For the translators, they don't need to handle excel or JSON files,
          they got their own web app.
        </p>
        <p>
          Following translation advancement is easier thanks to the dashboard.
        </p>
        <p>
          They got a common message syntax with ICU, which can be complex at
          times, but which allow them to handle edge cases for each language.
        </p>
      </MyNotes>
    </ConclusionTableSlide>
  )
);
