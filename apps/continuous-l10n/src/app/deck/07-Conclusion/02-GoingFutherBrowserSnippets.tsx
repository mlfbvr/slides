import React from 'react';
import CodeSlide from 'spectacle-code-slide';

export const GoingFutherBrowserSnippets = React.memo(
  ({ slideIndex }: { slideIndex?: number }) => (
    <CodeSlide
      slideIndex={slideIndex}
      transition={[]}
      lang="jsx"
      code={
        /* eslint-disable-next-line @typescript-eslint/no-var-requires */
        require('./02-GoingFutherBrowserSnippets/code.raw').default
      }
      ranges={[
        {
          loc: [0, 7],
          title: 'Set html lang',
          notes:
            'Good to know: you should always sync the lang attribute of the html tag. Your reader users will love you.',
        },
      ]}
    />
  )
);
