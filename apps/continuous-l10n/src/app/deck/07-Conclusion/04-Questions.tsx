import React from 'react';
import { Heading, Slide, Text } from 'spectacle';
import classNames from '../00-Intro/Intro.module.scss';
import sfeirLogo from '../../../assets/logo-sfeir.svg';
import { MyNotes } from '@mlfbvr/deck-components';
import { LINKS } from '../../links';
import { Links } from '../Links';

export const Questions = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide
      hash={slideIndex}
      transition={['zoom']}
      contentStyles={{
        width: '100%',
        maxWidth: '100%',
        height: '100%',
        maxHeight: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        padding: '4rem',
        color: 'white',
        background:
          'linear-gradient(to bottom, #171717 20%, transparent 150%), linear-gradient(to right, #00d4ff, #020024)',
      }}
    >
      <header className={classNames.header}>
        <img style={{ height: '2rem' }} src={sfeirLogo} alt="logo sfeir" />
      </header>
      <main className={classNames.main}>
        <Text margin="10px 0 0" textColor="white" bold italic>
          It was "Continuous Localization"
        </Text>
        <Heading
          size={1}
          caps
          lineHeight={1}
          textColor="white"
          className={classNames.title}
        >
          Any questions ?
        </Heading>
      </main>
      <footer className={classNames.footer}>
        <Links href={LINKS.SELF} />
        <Text margin="10px 0 0" textColor="white" bold>
          Maxence LEFEBVRE
        </Text>
      </footer>
      <MyNotes>
        <p>I'd like to thank all of you for listening to me today.</p>
        <p>Please raise your hand if you learned anything today.</p>
        <p>
          Again, thank you, I'd like to answer a few questions before going if
          there are any.
        </p>
      </MyNotes>
    </Slide>
  )
);
