import React from 'react';
import { TitleSlide } from '@mlfbvr/deck-components';

export const Conclusion = React.memo(() => <TitleSlide title="Conclusion" />);
