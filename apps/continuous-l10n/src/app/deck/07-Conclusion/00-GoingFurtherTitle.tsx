import React from 'react';
import { TitleSlide } from '@mlfbvr/deck-components';

export const GoingFurtherTitle = React.memo(() => (
  <TitleSlide title="Going further" />
));
