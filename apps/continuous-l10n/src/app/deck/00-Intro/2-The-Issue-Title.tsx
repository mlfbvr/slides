import React from 'react';
import { MyNotes, TitleSlide } from '@mlfbvr/deck-components';

export const TheIssueTitle = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <TitleSlide hash={slideIndex} title="The issue">
      <MyNotes>
        <p>So, why did I write this talk?</p>
        <p>
          Obviously, a few years ago, my team and I had to work on an
          internationalization feature.
        </p>
        <p>
          And we ran into a few issues, prompting us to improve our way of
          translating, but also of working with our translators.
        </p>
        <p>
          Today I want to show you how we achieved something we were kinda proud
          of. I'll do this by starting with a really naive/simple solution, and
          by incrementing over it.
        </p>
        <p>So let's start with the fundamental issue.</p>
      </MyNotes>
    </TitleSlide>
  )
);
