import React from 'react';
import { Image } from 'spectacle';
import { TheIssue } from './3-The-Issue';

import { MyNotes } from '@mlfbvr/deck-components';
import meme from '../../../assets/surprised_pikachu_meme.jpg';
import classNames from './The-Issue-Meme.module.scss';

export const TheIssueMeme = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <TheIssue slideIndex={slideIndex} overwriteTransitionDuration={0.001}>
      <div className={classNames.container}>
        <Image src={meme} />
      </div>
      <MyNotes>
        <p>this surprised pikachu meme face</p>
        <p>
          If you don't know this meme, it can be roughly translated to "No shit
          sherlock"
        </p>
      </MyNotes>
    </TheIssue>
  )
);
