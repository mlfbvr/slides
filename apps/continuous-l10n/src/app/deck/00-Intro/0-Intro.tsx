import React from 'react';
import { Heading, Slide, Text } from 'spectacle';

import { MyNotes } from '@mlfbvr/deck-components';
import sfeirLogo from '../../../assets/logo-sfeir.svg';
import classNames from './Intro.module.scss';
import { LINKS } from '../../links';
import { Links } from '../Links';

export const Intro = React.memo(({ slideIndex }: { slideIndex: number }) => (
  <Slide
    hash={slideIndex}
    transition={['zoom']}
    contentStyles={{
      width: '100%',
      maxWidth: '100%',
      height: '100%',
      maxHeight: '100%',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-between',
      alignItems: 'flex-start',
      padding: '4rem',
      color: 'white',
      background:
        'linear-gradient(to bottom, #171717 20%, transparent 150%), linear-gradient(to right, #00d4ff, #020024)',
    }}
  >
    <header className={classNames.header}>
      <img style={{ height: '2rem' }} src={sfeirLogo} alt="logo sfeir" />
    </header>
    <main className={classNames.main}>
      <Heading
        size={1}
        caps
        lineHeight={1}
        textColor="white"
        className={classNames.title}
      >
        Continuous Localization
      </Heading>
    </main>
    <footer className={classNames.footer}>
      <Links href={LINKS.SLIDES} />
      <Text margin="10px 0 0" textColor="white" bold>
        Maxence LEFEBVRE
      </Text>
    </footer>
    <MyNotes>
      <p>Hello everyone! I hope you are doing well!</p>
      <p>
        I am really glad to welcome all of you to my talk called "Continuous
        Localization".
      </p>
      <p>
        On the bottom of this slide, you will find a QR code and a shortlink to
        this presentation.
      </p>
      <p>Sorry I haven't had the time to make it 100% responsible.</p>
      <p>
        I am Maxence LEFEBVRE, I am a Senior Web Developer working at SFEIR and
        I will be your speaker today.
      </p>
      <p>
        This is a talk you may have already seen last year at DevFest Nantes &
        Lille, or online at React Global Summit.
      </p>
    </MyNotes>
  </Slide>
));
