import React from 'react';
import { Heading, Slide, Text } from 'spectacle';

export const NoStandard = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide
      hash={slideIndex}
      contentStyles={{
        width: '100%',
        maxWidth: '100%',
        height: '100%',
        maxHeight: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-evenly',
        alignItems: 'flex-start',
        padding: '4rem',
        color: 'white',
        background:
          'linear-gradient(to bottom, #171717 20%, transparent 150%), linear-gradient(to right, #00d4ff, #020024)',
      }}
    >
      <Heading
        textColor="white"
        style={{ width: '100%', wordBreak: 'break-word' }}
      >
        There is no standard way of managing an app&apos;s internationalization
        with React.
      </Heading>
      <Text textColor="white">
        Disclaimer: we are not about to present a standard nor propose one.
      </Text>
    </Slide>
  )
);
