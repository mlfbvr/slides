import { Intro } from './0-Intro';
import { TheIssueTitle } from './2-The-Issue-Title';
import { TheIssue } from './3-The-Issue';
import { TheIssueMeme } from './4-The-Issue-Meme';
import { FunUnits } from './5-Fun-Units';
import { Situation } from './6-Situation';
import { TheReturnOfTheIssue } from './7-The-Return-Of-The-Issue';
import { NoStandard } from './8-No-Standard';

export default [
  Intro,
  // Whoami,
  TheIssueTitle,
  TheIssue,
  TheIssueMeme,
  FunUnits,
  Situation,
  TheReturnOfTheIssue,
  NoStandard,
];
