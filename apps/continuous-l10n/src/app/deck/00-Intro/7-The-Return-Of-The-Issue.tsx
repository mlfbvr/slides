import React from 'react';
import { Heading } from 'spectacle';
import { MyNotes, TitleSlide } from '@mlfbvr/deck-components';

export const TheReturnOfTheIssue = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <TitleSlide hash={slideIndex} title="The issue - Part Two">
      <Heading textColor="primary" size={4} italic>
        The return of the issue
      </Heading>
      <MyNotes>
        To make things even simpler, we need to have another issue, obviously.
      </MyNotes>
    </TitleSlide>
  )
);
