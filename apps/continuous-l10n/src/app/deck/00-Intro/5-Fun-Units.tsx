import React from 'react';
import { Image, Slide, Text } from 'spectacle';

import funUnits from './../../../assets/units-are-fun.jpg';
import { MyNotes } from '@mlfbvr/deck-components';

export const FunUnits = React.memo(({ slideIndex }: { slideIndex: number }) => (
  <Slide hash={slideIndex} bgColor="primary">
    <Text textColor="secondary">
      Not everyone represents common concepts in the same way (such as dates,
      measurement units, etc...).
    </Text>
    <Image margin="5rem 0 0 0" src={funUnits} />
    <MyNotes>
      <p>
        And to make things even simpler, without mentioning the imperial system,
        since everyone already makes fun of that
      </p>
      <p>
        Other common concepts, such as dates are not represented the same all
        around Earth. Or the position of the currency in a price. Or how to
        separate thousands and decimals in a number.
      </p>
    </MyNotes>
  </Slide>
));
