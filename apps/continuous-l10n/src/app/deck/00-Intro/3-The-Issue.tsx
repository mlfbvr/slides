import React, { ReactNode } from 'react';
import { BlockQuote, Cite, Quote, Slide } from 'spectacle';
import { MyNotes } from '@mlfbvr/deck-components';

export const TheIssue = React.memo(
  ({
    slideIndex,
    children = null,
    overwriteTransitionDuration,
  }: {
    slideIndex: number;
    children?: ReactNode;
    overwriteTransitionDuration?: number;
  }) => (
    <Slide
      hash={slideIndex}
      transition={['fade']}
      transitionDuration={overwriteTransitionDuration}
      contentStyles={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        maxWidth: '100%',
        height: '100%',
        maxHeight: '100%',
        padding: '0 20%',
        background:
          'linear-gradient(to bottom, #171717 20%, transparent 150%), linear-gradient(to right, #00d4ff, #020024)',
      }}
    >
      <div>
        <BlockQuote>
          <Quote>Not everyone speaks the same language.</Quote>
          <Cite textColor="quaternary" margin="10px 0 0 30px">
            Sherlock Holmes
          </Cite>
        </BlockQuote>
      </div>
      {children}
      {!children && (
        <MyNotes>
          <p>"Not everyone speaks the same language."</p>
          <p>Sherlock Holmes.</p>
          <p>I've seen your faces in the audience and it's kinda like:</p>
        </MyNotes>
      )}
    </Slide>
  )
);
