import React from 'react';
import { Heading, Slide } from 'spectacle';
import classNames from './Intro.module.scss';
import { LINKS } from '../../links';
import { Links } from '../Links';

export const Whoami = React.memo(() => (
  <Slide transition={['zoom']} bgColor="primary">
    <Heading
      size={1}
      fit
      lineHeight={1}
      textColor="secondary"
      className={classNames.title}
    >
      $whoami
    </Heading>
    <Links href={LINKS.SLIDES} />
  </Slide>
));
