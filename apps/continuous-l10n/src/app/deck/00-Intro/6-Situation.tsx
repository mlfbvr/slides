import React from 'react';
import { bulletStyleType, Heading, List, ListItem, Slide } from 'spectacle';
import { MyNotes } from '@mlfbvr/deck-components';

export const Situation = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide hash={slideIndex} bgColor="primary">
      <Heading size={4} textColor="tertiary">
        Our need
      </Heading>
      <List bulletStyle={'💲' as bulletStyleType}>
        <ListItem margin="2rem 0 0 0" textSize="2rem">
          We MUST be able to reach a maximum of users across multiple countries
        </ListItem>
        <ListItem margin="2rem 0 0 0" textSize="2rem">
          It MUST not increase development time too much
        </ListItem>
        <ListItem margin="2rem 0 0 0" textSize="2rem">
          Adding and updating languages MUST be easy even after several months /
          years of development
        </ListItem>
      </List>
      <MyNotes>
        <p>So let's start a little roleplay between us.</p>
        <p>
          We will be creating a startup. What do we want most ? That's right,
          money.
        </p>
        <p>
          And to make money, we don't want to limit our userbase to France or
          our little country. We want to expand our reach worldwide.
        </p>
        <p>
          But since we are after the money, we don't want to spend too much,
          including in this internationalization feature.
        </p>
        <p>
          So, we want to be able to expand our reach, but not increasing
          development time too much for our features, nor spending too much for
          adding a new language, or updating existing translations.
        </p>
      </MyNotes>
    </Slide>
  )
);
