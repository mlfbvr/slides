import React from 'react';
import { MyNotes, TitleSlide } from '@mlfbvr/deck-components';

export const SyncSolutionTitle = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <TitleSlide
      title="Adding gettext workflow to our solution"
      hash={slideIndex}
    >
      <MyNotes>
        <p>So let's add a gettext workflow to our solution.</p>
      </MyNotes>
    </TitleSlide>
  )
);
