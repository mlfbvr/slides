import { SyncSolutionTitle } from './00-SyncSolutionTitle';
import { SyncSolutionWorkflow } from './01-SyncSolutionWorkflow';
import { SyncSolutionDeclareMessages } from './02-SyncSolutionDeclareMessages';
import { SyncSolutionExtractMessages } from './03-SyncSolutionExtractMessages';
import { SyncSolutionMessagesJson } from './04-SyncSolutionMessagesJson';
import { SyncSolutionJsonToPot } from './05-SyncSolutionJsonToPot';
import { SyncSolutionMessagesPot } from './06-SyncSolutionMessagesPot';
import { SyncSolutionCmds } from './07-SyncSolutionCmds';
import { SyncSolutionPoFile } from './08-SyncSolutionPoFile';
import { SyncSolutionWebpackLoader } from './09-SyncSolutionWebpackLoader';
import { SyncSolutionImportPo } from './10-SyncSolutionImportPo';
import { SyncSolutionDownsides } from './11-SyncSolutionDownsides';
import { SyncSolutionHooksTitle } from './12-SyncSolutionHooksTitle';
import { SyncSolutionHooks } from './13-SyncSolutionHooks';
import { SyncSolutionDX } from './14-SyncSolutionDX';
import { SyncSolutionTX } from './15-SyncSolutionTX';

export default [
  SyncSolutionTitle,
  SyncSolutionWorkflow,
  SyncSolutionDeclareMessages,
  SyncSolutionExtractMessages,
  SyncSolutionMessagesJson,
  SyncSolutionJsonToPot,
  SyncSolutionMessagesPot,
  SyncSolutionCmds,
  SyncSolutionPoFile,
  SyncSolutionWebpackLoader,
  SyncSolutionImportPo,
  SyncSolutionDownsides,
  SyncSolutionHooksTitle,
  SyncSolutionHooks,
  SyncSolutionDX,
  SyncSolutionTX,
];
