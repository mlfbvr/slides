import React from 'react';
import CodeSlide from 'spectacle-code-slide';

export const SyncSolutionMessagesJson = React.memo(
  ({ slideIndex }: { slideIndex?: number }) => (
    <CodeSlide
      slideIndex={slideIndex}
      transition={[]}
      lang="json"
      code={
        /* eslint-disable-next-line @typescript-eslint/no-var-requires */
        require('./04-SyncSolutionMessagesJson/code.raw.example').default
      }
      ranges={[
        {
          loc: [0, 10],
          title: 'messages.json',
          notes:
            "The resulting messages.json looks like that. Now let's transform it to a pot file.",
        },
      ]}
    />
  )
);
