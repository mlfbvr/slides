import React from 'react';
import CodeSlide from 'spectacle-code-slide';

export const SyncSolutionMessagesPot = React.memo(
  ({ slideIndex }: { slideIndex?: number }) => (
    <CodeSlide
      slideIndex={slideIndex}
      transition={[]}
      lang="bash"
      code={
        /* eslint-disable-next-line @typescript-eslint/no-var-requires */
        require('./06-SyncSolutionMessagesPot/code.raw.pot').default
      }
      ranges={[
        { loc: [0, 7], title: 'messages.pot' },
        { loc: [9, 24], title: 'messages.pot' },
      ]}
    />
  )
);
