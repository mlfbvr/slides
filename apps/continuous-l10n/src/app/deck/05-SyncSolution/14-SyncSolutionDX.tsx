import React from 'react';
import { ConclusionTableSlide, MyNotes } from '@mlfbvr/deck-components';
import { DX } from '../../constants';

export const SyncSolutionDX = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <ConclusionTableSlide
      hash={slideIndex}
      title="Developers"
      criterias={DX.SYNC_SOLUTION}
      previous={DX.INTL}
    >
      <MyNotes>
        <p>For the DX, we did some trade offs:</p>
        <p>
          Implementation isn't as simple as before, but let's be honest, it's
          not that difficult either.
        </p>
        <p>
          We really improved our scalability, we don't need to waste 2 days a
          week to synchronize our translation files. And adding a new language
          is simplified as running one command line.
        </p>
        <p>
          We did not improve our translation lag though, we still need to use a
          mail based flow with our translators.
        </p>
      </MyNotes>
    </ConclusionTableSlide>
  )
);
