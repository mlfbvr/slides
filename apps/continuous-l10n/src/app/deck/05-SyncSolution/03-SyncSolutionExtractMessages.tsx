import React from 'react';
import CodeSlide from 'spectacle-code-slide';

export const SyncSolutionExtractMessages = React.memo(
  ({ slideIndex }: { slideIndex?: number }) => (
    <CodeSlide
      slideIndex={slideIndex}
      transition={[]}
      lang="js"
      code={
        /* eslint-disable-next-line @typescript-eslint/no-var-requires */
        require('./03-SyncSolutionExtractMessages/code.raw').default
      }
      ranges={[
        {
          loc: [0, 2],
          title: 'Extract messages',
          note: 'Using a node library',
          notes:
            'We then write a little script which will extract our messages in our codebase. We use the extract-react-intl-messages library.',
        },
        {
          loc: [6, 11],
          title: 'Extract messages',
          notes: 'We use this function to extract the messages.',
        },
        {
          loc: [8, 9],
          title: 'Optimize',
          note: 'Optimize with file suffix',
          notes:
            'To make sure it doesnt analyse all files, we used a convention. Messages will be defined in files suffixed by dot messages.',
        },
        {
          loc: [12, 19],
          title: 'Transform output',
          notes: 'We then quickly transform the output.',
        },
        {
          loc: [22, 26],
          title: 'Write to file',
          notes: 'And write the messages.json file.',
        },
        {
          loc: [28, 31],
          title: 'Handle process errors',
          notes:
            'Never forget to handle the errors. You could also print the error in the catch callback.',
        },
      ]}
    />
  )
);
