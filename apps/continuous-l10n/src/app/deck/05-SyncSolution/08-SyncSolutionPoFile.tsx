import React from 'react';
import CodeSlide from 'spectacle-code-slide';

export const SyncSolutionPoFile = React.memo(
  ({ slideIndex }: { slideIndex?: number }) => (
    <CodeSlide
      slideIndex={slideIndex}
      transition={[]}
      lang="bash"
      code={
        /* eslint-disable-next-line @typescript-eslint/no-var-requires */
        require('./08-SyncSolutionPoFile/code.raw.po').default
      }
      ranges={[
        { loc: [0, 11], title: 'Translate' },
        { loc: [13, 28], title: 'Translate' },
      ]}
    />
  )
);
