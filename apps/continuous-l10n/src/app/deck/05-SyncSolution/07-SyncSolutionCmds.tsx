import React from 'react';
import CodeSlide from 'spectacle-code-slide';

export const SyncSolutionCmds = React.memo(
  ({ slideIndex }: { slideIndex?: number }) => (
    <CodeSlide
      slideIndex={slideIndex}
      transition={[]}
      lang="bash"
      code={
        /* eslint-disable-next-line @typescript-eslint/no-var-requires */
        require('./07-SyncSolutionCmds/code.raw.sh').default
      }
      ranges={[
        { loc: [0, 4], title: 'Create' },
        { loc: [7, 11], title: 'Or update' },
      ]}
    />
  )
);
