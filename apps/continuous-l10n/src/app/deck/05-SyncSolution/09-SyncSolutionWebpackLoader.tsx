import React from 'react';
import CodeSlide from 'spectacle-code-slide';

export const SyncSolutionWebpackLoader = React.memo(
  ({ slideIndex }: { slideIndex?: number }) => (
    <CodeSlide
      slideIndex={slideIndex}
      transition={[]}
      lang="js"
      code={
        /* eslint-disable-next-line @typescript-eslint/no-var-requires */
        require('./09-SyncSolutionWebpackLoader/code.raw').default
      }
      ranges={[
        {
          loc: [0, 14],
          title: 'Webpack loader',
          notes:
            "Let's thank my brother for the react-intl-po-loader for webpack which allows you to import po file from your code.",
        },
      ]}
    />
  )
);
