import React from 'react';
import { MyNotes, TitleSlide } from '@mlfbvr/deck-components';

export const SyncSolutionHooksTitle = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <TitleSlide
      title="Syncing translations files with the sources"
      hash={slideIndex}
    >
      <MyNotes>
        <p>Let's automate the translation files synchronizations.</p>
      </MyNotes>
    </TitleSlide>
  )
);
