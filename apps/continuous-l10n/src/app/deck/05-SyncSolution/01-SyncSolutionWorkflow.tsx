import React from 'react';
import { Heading, Image, Slide } from 'spectacle';

import workflow from '../../../assets/react-intl-po_workflow.png';
import { MyNotes } from '@mlfbvr/deck-components';

export const SyncSolutionWorkflow = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide bgColor="primary" hash={slideIndex}>
      <Heading size={3} textColor="tertiary">
        Workflow overview
      </Heading>

      <Image margin="6rem auto 0" src={workflow} />

      <MyNotes>
        <p>So lets review the workflow:</p>
        <p>We will start by declaring our messages in our codebase.</p>
        <p>
          Use some tooling to extract them to a messages.json which we will
          transform into a pot file.
        </p>
        <p>
          Use some tooling to synchronize our translations files and send them
          to our translators.
        </p>
        <p>
          Wait for the translators to send them back and integrate them in our
          codebase.
        </p>
        <p>
          Then use some tooling to transform those PO files to JSON files so
          that our codebase could import them.
        </p>
        <p>Ready ? Let's go</p>
      </MyNotes>
    </Slide>
  )
);
