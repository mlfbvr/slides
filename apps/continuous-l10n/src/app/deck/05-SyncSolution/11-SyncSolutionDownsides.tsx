import React from 'react';
import { Heading, List, ListItem, Slide } from 'spectacle';
import { MyNotes } from '@mlfbvr/deck-components';

export const SyncSolutionDownsides = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide bgColor="primary" hash={slideIndex}>
      <Heading size={3} textColor="tertiary" margin="0 0 2rem">
        Downsides
      </Heading>

      <List margin="4rem 0 0">
        <ListItem>PO files must be synced manually.</ListItem>
        <ListItem>
          Divert use of <b>msgctxt</b>.
        </ListItem>
        <ListItem>Need to write custom scripts.</ListItem>
        <ListItem>String locations are wrong.</ListItem>
      </List>

      <MyNotes>
        <p>There are still some downsides.</p>
        <p>We need to sync the PO files "manually".</p>
        <p>We had to divert the use of msgctx</p>
        <p>We had to write some custom scripts / configuration.</p>
        <p>
          I haven't shown you but the messages location are wrong in the
          resulting po file.
        </p>
      </MyNotes>
    </Slide>
  )
);
