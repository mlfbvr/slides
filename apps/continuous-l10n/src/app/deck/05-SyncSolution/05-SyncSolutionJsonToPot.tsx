import React from 'react';
import CodeSlide from 'spectacle-code-slide';

export const SyncSolutionJsonToPot = React.memo(
  ({ slideIndex }: { slideIndex?: number }) => (
    <CodeSlide
      slideIndex={slideIndex}
      transition={[]}
      lang="bash"
      code={
        /* eslint-disable-next-line @typescript-eslint/no-var-requires */
        require('./05-SyncSolutionJsonToPot/code.raw.sh').default
      }
      ranges={[
        {
          loc: [0, 5],
          title: 'Extract',
          notes: "I am still lazy, so let's use another library.",
        },
        {
          loc: [4, 5],
          title: 'Set msgctxt to id',
          note: 'This is needed to retrieve the id when parsing the PO back to JSON',
          notes:
            'I was saying earlier that I needed to divert some features, here I divert the use of the PO context to store the message id, so that I can convert back from the PO file later.',
        },
      ]}
    />
  )
);
