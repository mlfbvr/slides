import { defineMessages } from 'react-intl';

export const messages = defineMessages({
  HELLO: {
    id: 'hello',
    defaultMessage: 'Hello',
  },
  WORLD: {
    id: 'world',
    defaultMessage: 'World',
  },
});

<FormattedMessage {...messages.HELLO} />;
