msginit \
  -i messages.pot \
  -o fr.po \
  --locale=fr

###

msgmerge \
  --update \
  fr.po \
  messages.pot
