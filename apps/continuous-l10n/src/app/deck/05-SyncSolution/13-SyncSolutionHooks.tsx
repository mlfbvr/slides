import React from 'react';
import CodeSlide from 'spectacle-code-slide';

export const SyncSolutionHooks = React.memo(
  ({ slideIndex }: { slideIndex?: number }) => (
    <CodeSlide
      slideIndex={slideIndex}
      transition={[]}
      lang="js"
      code={
        /* eslint-disable-next-line @typescript-eslint/no-var-requires */
        require('./13-SyncSolutionHooks/code.raw').default
      }
      ranges={[
        {
          loc: [0, 8],
          title: '.huskyrc.js',
          note: 'yarn add --dev husky',
          notes:
            'We install husky, which allows us to add some scripts to the git hooks',
        },
        { loc: [2, 3], title: 'When commiting' },
        { loc: [3, 4], title: 'Sync PO files' },
        { loc: [4, 5], title: 'Add updated PO files' },
      ]}
    />
  )
);
