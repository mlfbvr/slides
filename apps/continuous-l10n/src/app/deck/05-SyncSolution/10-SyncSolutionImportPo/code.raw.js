import messagesEn from 'en.po';
import messagesFr from 'fr.po';

const locales = {
  en: {
    messages: messagesEn,
  },
  fr: {
    messages: messagesFr,
  },
};
