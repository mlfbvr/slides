import React from 'react';
import { ConclusionTableSlide, MyNotes } from '@mlfbvr/deck-components';
import { TX } from '../../constants';

export const SyncSolutionTX = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <ConclusionTableSlide
      title="Translators"
      criterias={TX.SYNC_SOLUTION}
      previous={TX.INTL}
      hash={slideIndex}
    >
      <MyNotes>
        <p>
          As for the translators, they don't need to handle JSON or Excel files.
        </p>
        <p>
          They can use 3rd party tools like POEdit to translate our PO files.
          Those tools also allow them to pre-translate messages with google
          translate or deepl!
        </p>
        <p>
          Also, PO files allow them to tag translations as fuzzy! Collaboration
          is made really easier for them.
        </p>
      </MyNotes>
    </ConclusionTableSlide>
  )
);
