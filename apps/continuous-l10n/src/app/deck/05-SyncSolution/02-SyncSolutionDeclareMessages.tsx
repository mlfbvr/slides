import React from 'react';
import CodeSlide from 'spectacle-code-slide';

export const SyncSolutionDeclareMessages = React.memo(
  ({ slideIndex }: { slideIndex?: number }) => (
    <CodeSlide
      slideIndex={slideIndex}
      transition={[]}
      lang="jsx"
      code={
        /* eslint-disable-next-line @typescript-eslint/no-var-requires */
        require('./02-SyncSolutionDeclareMessages/code.raw').default
      }
      ranges={[
        {
          loc: [0, 12],
          title: 'Declare messages',
          notes: (
            <>
              <p>
                We start by using the defineMessages function exported from
                react-intl
              </p>
              <p>
                This function is in reality a babel macro: it will do nothing in
                production, only return the input object.
              </p>
              <p>
                At buildtime however, it will add some metadata to the babel AST
                to tell us that there are translations to extract here.
              </p>
            </>
          ),
        },
        {
          loc: [4, 5],
          title: 'Unique id',
          notes:
            'Make sure you use an unique id. If you fear conflicts, you can let the id undefined and tell the library to generate unique hashes for you.',
        },
        {
          loc: [5, 6],
          title: 'Default message',
          note: 'In the app default locale',
          notes:
            'Define a default message in the app default locale. It will be the string to translate in the resulting po files.',
        },
      ]}
    />
  )
);
