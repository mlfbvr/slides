import React from 'react';
import CodeSlide from 'spectacle-code-slide';

export const SyncSolutionImportPo = React.memo(
  ({ slideIndex }: { slideIndex?: number }) => (
    <CodeSlide
      slideIndex={slideIndex}
      transition={[]}
      lang="js"
      code={
        /* eslint-disable-next-line @typescript-eslint/no-var-requires */
        require('./10-SyncSolutionImportPo/code.raw').default
      }
      ranges={[
        { loc: [0, 11], title: 'Import', notes: 'In our codebase' },
        {
          loc: [0, 2],
          title: 'Directly import po',
          notes: 'We can directly import the po files',
        },
        {
          loc: [5, 6],
          title: 'Set locales translations',
          notes:
            'And set them as our translated messages in the translation dictionary',
        },
      ]}
    />
  )
);
