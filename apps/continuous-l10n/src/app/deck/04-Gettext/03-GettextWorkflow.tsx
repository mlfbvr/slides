import React from 'react';
import { Heading, Slide } from 'spectacle';

import Workflow from '../../../assets/gettext-workflow.svg';
import { MyNotes } from '@mlfbvr/deck-components';

export const GettextWorkflow = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide bgColor="primary" hash={slideIndex}>
      <Heading size={3} textColor="tertiary">
        gettext workflow
      </Heading>

      <div style={{ margin: '6rem auto 0' }}>
        <img src={Workflow} alt="gettext workflow" width="100%" height="auto" />
      </div>

      <MyNotes>
        <p>So the workflow of a gettext based solution would be:</p>
        <p>First I declare the messages in my codebase.</p>
        <p>I use a command tool to extract those messages to POT file</p>
        <p>Another command tool to create or update PO files</p>
        <p>
          I then send those PO files to a translator and put them back in my
          codebase once translated.
        </p>
        <p>
          I import those files in my code, and then use the ICU engine to format
          them and display them in my app.
        </p>
      </MyNotes>
    </Slide>
  )
);
