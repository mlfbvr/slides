import React from 'react';
import { Heading, List, ListItem, Slide, Text } from 'spectacle';
import { MyNotes } from '@mlfbvr/deck-components';

export const GettextCons = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide bgColor="primary" hash={slideIndex}>
      <Heading size={3} textColor="tertiary" margin="0 0 4rem">
        Pros and Cons
      </Heading>

      <Heading size={4} textAlign="left">
        Cons
      </Heading>

      <List margin="2rem 0 0">
        <ListItem style={{ fontSize: '2.25rem' }}>
          Native plural support does not handle decimals.
        </ListItem>
        <ListItem style={{ fontSize: '2.25rem' }}>
          Native message extraction only for C source files.
        </ListItem>
      </List>

      <Text textAlign="left" margin="3rem 0 0">
        <b>In our case:</b>
      </Text>
      <List margin="1rem 0 0">
        <ListItem style={{ fontSize: '2.25rem' }}>
          No Node implementation.
        </ListItem>
        <ListItem style={{ fontSize: '2.25rem' }}>
          Need to divert the use of some features.
        </ListItem>
      </List>

      <MyNotes>
        <p>
          In terms of cons. The native implementation doesn't handle decimals in
          its plural support. The message extraction natively works only with C
          messages.
        </p>
        <p>
          In our case, there are no node implementation, we need to install the
          package on our OS before using it. Also, we will have to divert the
          use of some features of the PO files as you will see in an instant.
        </p>
      </MyNotes>
    </Slide>
  )
);
