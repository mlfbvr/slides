import React from 'react';
import { Heading, Slide, Text } from 'spectacle';
import { CodeDark, MyNotes } from '@mlfbvr/deck-components';

export const GettextMsgMerge = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide bgColor="primary">
      <Heading size={3} textColor="tertiary">
        Merge
      </Heading>

      <Text textAlign="left" margin="4rem 0">
        Synchronize a translation file with up-to-date translations:
      </Text>

      <div style={{ marginTop: '4rem' }}>
        <CodeDark>
          {/* eslint-disable-next-line @typescript-eslint/no-var-requires */}
          {require('./06-GettextMsgMerge/msgmerge.raw.sh').default}
        </CodeDark>
      </div>
      <MyNotes>
        <p>And updating an existing translation file with one command also.</p>
      </MyNotes>
    </Slide>
  )
);
