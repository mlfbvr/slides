import React from 'react';
import { Heading, List, ListItem, Slide } from 'spectacle';
import { MyNotes } from '@mlfbvr/deck-components';

export const GettextPros = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide bgColor="primary" hash={slideIndex}>
      <Heading size={3} textColor="tertiary" margin="0 0 4rem">
        Pros and Cons
      </Heading>

      <Heading size={4} textAlign="left">
        Pros
      </Heading>

      <List margin="2rem 0 0">
        <ListItem style={{ fontSize: '2.25rem' }}>
          Create new translation files.
        </ListItem>
        <ListItem style={{ fontSize: '2.25rem' }}>
          Synchronize translation files.
        </ListItem>
        <ListItem style={{ fontSize: '2.25rem' }}>
          Lots of external tools compatible with PO format.
        </ListItem>
      </List>

      <MyNotes>
        <p>In term of pros of the gettext solution</p>
        <p>It is really easy to scale our languages</p>
        <p>
          And there are a lot of tools which handle the PO format, desktop tools
          like POEdit or web based tools like Crowdin
        </p>
      </MyNotes>
    </Slide>
  )
);
