import React from 'react';
import { Heading, List, ListItem, Slide } from 'spectacle';
import { MyNotes } from '@mlfbvr/deck-components';

export const WhatsGettext = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide bgColor="primary" hash={slideIndex}>
      <Heading size={3} textColor="tertiary" margin="0 0 2rem">
        gettext?
      </Heading>

      <List margin="4rem 0 0">
        <ListItem>Conventions for naming directories, files...</ListItem>
        <ListItem>Tools to extract C code messages.</ListItem>
        <ListItem>
          <b>Tools to manage translation files.</b>
        </ListItem>
      </List>
      <MyNotes>
        <p>What's gettext you are asking ?</p>
        <p>It's a convention for naming directories, files... in C.</p>
        <p>
          But it comes with a set of tools to handle translations in C :
          extracting messages from code and manage the translations files.
        </p>
      </MyNotes>
    </Slide>
  )
);
