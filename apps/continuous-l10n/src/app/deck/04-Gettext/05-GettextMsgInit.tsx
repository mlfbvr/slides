import React from 'react';
import { Heading, Slide, Text } from 'spectacle';
import { CodeDark, MyNotes } from '@mlfbvr/deck-components';

export const GettextMsgInit = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide bgColor="primary" hash={slideIndex}>
      <Heading size={3} textColor="tertiary">
        New translation file
      </Heading>

      <Text textAlign="left" margin="4rem 0">
        Create a translation file for a new language with empty translations:
      </Text>

      <div style={{ marginTop: '4rem' }}>
        <CodeDark>
          {/* eslint-disable-next-line @typescript-eslint/no-var-requires */}
          {require('./05-GettextMsgInit/msginit.raw.sh').default}
        </CodeDark>
      </div>
      <MyNotes>
        <p>
          As you can see, with one command, I can create a translation file for
          french from a pot file.
        </p>
      </MyNotes>
    </Slide>
  )
);
