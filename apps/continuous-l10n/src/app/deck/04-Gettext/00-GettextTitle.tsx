import React from 'react';
import { MyNotes, TitleSlide } from '@mlfbvr/deck-components';

export const GettextTitle = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <TitleSlide title="gettext" hash={slideIndex}>
      <MyNotes>
        <p>Let's focus on the scalability now.</p>
        <p>I'd like to introduce to you the gettext library.</p>
      </MyNotes>
    </TitleSlide>
  )
);
