import { GettextTitle } from './00-GettextTitle';
import { WhatsGettext } from './01-WhatsGettext';
import { GettextFileFormats } from './02-GettextFileFormats';
import { GettextWorkflow } from './03-GettextWorkflow';
import { PoStructure } from './04-PoStructure';
import { GettextMsgInit } from './05-GettextMsgInit';
import { GettextMsgMerge } from './06-GettextMsgMerge';
import { GettextPros } from './07-GettextPros';
import { GettextCons } from './08-GettextCons';

export default [
  GettextTitle,
  WhatsGettext,
  GettextFileFormats,
  GettextWorkflow,
  PoStructure,
  GettextMsgInit,
  GettextMsgMerge,
  GettextPros,
  GettextCons,
];
