import React from 'react';
import CodeSlide from 'spectacle-code-slide';

export const PoStructure = React.memo(
  ({ slideIndex }: { slideIndex?: number }) => (
    <CodeSlide
      slideIndex={slideIndex}
      transition={[]}
      lang="bash"
      code={
        /* eslint-disable-next-line @typescript-eslint/no-var-requires */
        require('./04-PoStructure/code.raw.po').default
      }
      ranges={[
        {
          loc: [0, 12],
          title: 'Header',
          notes:
            'The structure of a PO file is quite verbose. We start with this large header.',
        },
        {
          loc: [10, 11],
          title: 'File language',
          notes: 'It specifies the language of this file.',
        },
        {
          loc: [11, 12],
          title: 'Language plurals',
          notes:
            'It also reminds the plural rules. In our case we wont be using it, and it does not support decimal numbers.',
        },
        {
          loc: [13, 41],
          title: 'Messages',
          notes: 'We then have multiple messages blocks',
        },
        {
          loc: [13, 15],
          title: 'Message comments',
          notes: 'It has message comments',
        },
        {
          loc: [15, 17],
          title: 'Message & translation',
          notes: 'And the default message and its translation',
        },
        {
          loc: [13, 14],
          title: 'Developer comment',
          notes:
            'You can provide a developer comment if necessary with hashdot',
        },
        {
          loc: [14, 15],
          title: 'Source location',
          notes: 'The location of the message is found behind hashcolon',
        },
        {
          loc: [15, 16],
          title: 'Original string',
          notes: 'The original message is behind msgid',
        },
        {
          loc: [16, 17],
          title: 'Translation',
          notes: 'and the translated one behing msgstr',
        },
        {
          loc: [18, 29],
          title: 'Message context',
          notes:
            'You can also add a context to the message, like here, where free can be in the sense of freedom or a product having no cost. ' +
            'This example was used in a South Park episode which was quite hard to translate to other languages.',
        },
        {
          loc: [32, 35],
          title: 'Fuzzy messages',
          notes: (
            <>
              <p>
                Behind hashcomma you can specify fuzzy. This allows the
                translator to tag the translation as fuzzy.
              </p>
              <p>
                This means that the translator is not quite sure of his
                translation, for example here, translating I'm hungry to
                Bonjour.
              </p>
              <p>
                This allows for easier translations reviews, to tell other
                translators to review this one in priority.'
              </p>
            </>
          ),
        },
        {
          loc: [36, 41],
          title: 'Obsolete messages',
          notes:
            'Finally, you can also comment blocks with hashtilde, for currently obsolete messages.',
        },
      ]}
    />
  )
);
