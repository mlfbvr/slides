import React from 'react';
import { Heading, List, ListItem, Slide } from 'spectacle';
import { MyNotes } from '@mlfbvr/deck-components';

export const GettextFileFormats = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide bgColor="primary" hash={slideIndex}>
      <Heading size={3} textColor="tertiary" margin="0 0 2rem">
        gettext
      </Heading>

      <List margin="4rem 0 0">
        <ListItem>File formats (PO/POT).</ListItem>
        <ListItem>Commands to create new translation files.</ListItem>
        <ListItem>Commands to update translation files.</ListItem>
      </List>
      <MyNotes>
        <p>It defines two file formats for the translations files.</p>
        <p>
          PO stands for Portable Object. There is one PO file for supported
          language
        </p>
        <p>
          There is also a POT, for PO Template, file which helps us
          synchronizing all the PO files.
        </p>
        <p>
          The gettext library provide commands to create or update translation
          files from a POT
        </p>
      </MyNotes>
    </Slide>
  )
);
