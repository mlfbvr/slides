import React from 'react';
import { Text } from 'spectacle';
import QRCode from 'qrcode.react';
import classNames from './Links.module.scss';

export const Links = React.memo(
  ({ href, label = href }: { href: string; label?: string }) => (
    <Text textColor="white" className={classNames.link}>
      <QRCode size={46} bgColor="transparent" value={href} />
      <a target="_blank" rel="noopener noreferrer" href={href}>
        {label}
      </a>
    </Text>
  )
);
