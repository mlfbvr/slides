import React from 'react';
import { ConclusionTableSlide, MyNotes } from '@mlfbvr/deck-components';
import { DX } from '../../constants';

export const ContinuousDX = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <ConclusionTableSlide
      hash={slideIndex}
      title="Developers"
      criterias={DX.CONTINUOUS}
      previous={DX.SYNC_SOLUTION}
    >
      <MyNotes>
        We improved our email based flow by using a WebUI for our translators.
        They can connect anytime and add a few translations, it will
        automatically open a PR to our repository with updated translations.
      </MyNotes>
    </ConclusionTableSlide>
  )
);
