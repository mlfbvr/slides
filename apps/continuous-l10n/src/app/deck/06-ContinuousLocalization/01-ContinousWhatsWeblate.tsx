import React from 'react';
import { Heading, Image, List, ListItem, Slide } from 'spectacle';

import weblateLogo from '../../../assets/weblate-logo.png';
import { MyNotes } from '@mlfbvr/deck-components';

export const ContinuousWhatsWeblate = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide bgColor="primary" hash={slideIndex}>
      <Heading size={3} textColor="tertiary">
        Weblate
      </Heading>

      <Image src={weblateLogo} width="15rem" margin="2rem auto" />

      <List>
        <ListItem style={{ fontSize: '01-GoingFurtherBundleSplitting.15rem' }}>
          Open source web application to manage translations.
        </ListItem>
        <ListItem style={{ fontSize: '01-GoingFurtherBundleSplitting.15rem' }}>
          Can be synced with a VCS repository.
        </ListItem>
        <ListItem style={{ fontSize: '01-GoingFurtherBundleSplitting.15rem' }}>
          Can open pull requests on GitHub and GitLab.
        </ListItem>
      </List>

      <MyNotes>
        <p>Weblate. Weblate? Weblate.</p>
        <p>
          Weblate is an open source web application to manage translations.
          There are alternatives of course. Like Crowdin, Phrase, etc...
        </p>
        <p>
          Thanks my mate Sylvain to make it compatible with Kubernetes and
          Openshift.
        </p>
        <p>
          Weblate can be synced with your Git repository, and on GitHub and
          GitLab, open pull requests.
        </p>
      </MyNotes>
    </Slide>
  )
);
