import React from 'react';
import { Heading, List, ListItem, Slide } from 'spectacle';
import { MyNotes } from '@mlfbvr/deck-components';

export const ContinuousWeblateCons = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide bgColor="primary" hash={slideIndex}>
      <Heading size={3} textColor="tertiary" margin="0 0 4rem">
        Pros and Cons
      </Heading>

      <Heading size={4} textAlign="left">
        Cons
      </Heading>

      <List margin="2rem 0 0">
        <ListItem margin="1rem 0">
          Pull requests not available on all VCS.
        </ListItem>
        <ListItem margin="1rem 0">UI could be more intuitive.</ListItem>
        <ListItem margin="1rem 0">No SSO.</ListItem>
      </List>

      <MyNotes>
        <p>In terms of cons...</p>
        <p>You can't open pull requests on Azure Devops</p>
        <p>
          The UI could be more intuitive, which is a way to say that it's not
          that great
        </p>
        <p>
          There is no SSO. This could be a real con if you want to crowdsource
          the translations with your employees, but with external translators,
          you don't want to pay a licence in your organization for them.
        </p>
      </MyNotes>
    </Slide>
  )
);
