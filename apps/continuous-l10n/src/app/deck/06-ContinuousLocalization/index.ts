import { ContinousTitle } from './00-ContinousTitle';
import { ContinuousWhatsWeblate } from './01-ContinousWhatsWeblate';
import { ContinuousWeblateDashboard } from './02-ContinuousWeblateDashboard';
import { ContinuousWeblatePros } from './03-ContinuousWeblatePros';
import { ContinuousWeblateCons } from './04-ContinuousWeblateCons';
import { ContinuousDX } from './05-ContinuousDX';
import { ContinuousTX } from './06-ContinuousTX';

export default [
  ContinousTitle,
  ContinuousWhatsWeblate,
  ContinuousWeblateDashboard,
  ContinuousWeblatePros,
  ContinuousWeblateCons,
  ContinuousDX,
  ContinuousTX,
];
