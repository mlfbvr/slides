import React from 'react';
import { Heading } from 'spectacle';
import { MyNotes, TitleSlide } from '@mlfbvr/deck-components';

export const ContinousTitle = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <TitleSlide hash={slideIndex} title="The Continous Localization part">
      <Heading caps textAlign="left" textColor="primary" size={6} italic>
        of the Continuous Localization
      </Heading>
      <MyNotes>
        <p>Now I hear you.</p>
        <p>Maxence, I've come to heard about continuous localization.</p>
        <p>
          I've learned a lot of things but where is the continuous in all that?
        </p>
        <p>Thank you for your patience, let's dive into that.</p>
      </MyNotes>
    </TitleSlide>
  )
);
