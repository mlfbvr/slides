import React from 'react';
import { ConclusionTableSlide, MyNotes } from '@mlfbvr/deck-components';
import { TX } from '../../constants';

export const ContinuousTX = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <ConclusionTableSlide
      title="Translators"
      criterias={TX.CONTINUOUS}
      previous={TX.SYNC_SOLUTION}
      hash={slideIndex}
    >
      <MyNotes>
        <p>
          The translators don't need any tooling anymore. Only credentials to
          our web ui.
        </p>
      </MyNotes>
    </ConclusionTableSlide>
  )
);
