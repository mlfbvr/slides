import React from 'react';
import { Heading, Image, Slide } from 'spectacle';

import dashboard from '../../../assets/dashboard.png';
import { MyNotes } from '@mlfbvr/deck-components';

export const ContinuousWeblateDashboard = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide bgColor="primary" hash={slideIndex}>
      <Heading size={3} textColor="tertiary">
        Translations summary
      </Heading>

      <div style={{ margin: '4rem auto 0', border: '1px solid #cdd0d3' }}>
        <Image src={dashboard} margin="0" />
      </div>
      <MyNotes>
        <p>
          Here is an example of a dashboard to see the completion of the
          translations for each language of your app. Your Product Manager will
          be quite happy to have this kind of view. Your translators too.
        </p>
        <p>
          It tells you the completion rate, how many are tagged as fuzzy (to
          review), and how many are not translated at all.
        </p>
        <p>
          This allow you to let your own collaborators to help you translate
          your app, to crowdsource in a way your translations.
        </p>
      </MyNotes>
    </Slide>
  )
);
