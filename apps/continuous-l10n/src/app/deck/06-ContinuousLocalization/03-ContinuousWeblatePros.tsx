import React from 'react';
import { Heading, List, ListItem, Slide } from 'spectacle';
import { MyNotes } from '@mlfbvr/deck-components';

export const ContinuousWeblatePros = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide bgColor="primary" hash={slideIndex}>
      <Heading size={3} textColor="tertiary" margin="0 0 4rem">
        Pros and Cons
      </Heading>

      <Heading size={4} textAlign="left">
        Pros
      </Heading>

      <List margin="2rem 0 0">
        <ListItem margin="1rem 0">Free if on-premise.</ListItem>
        <ListItem margin="1rem 0">
          Integration with GitHub/GitLab/Bitbucket.
        </ListItem>
        <ListItem margin="1rem 0">Advancement dashboard.</ListItem>
        <ListItem margin="1rem 0">
          Open source and responsive maintainer.
        </ListItem>
      </List>

      <MyNotes>
        <p>
          Weblate is free if on-premise, which is nice for our startup. Remember
          we don't want to spend too much.
        </p>
        <p>It has integrations with a few git tools.</p>
        <p>You have an advancement dashboard for your product manager</p>
        <p>
          It is open source, and its maintainer is quite reactive. When we had
          to make it compatible with OpenShift, it took only a few days, since
          the maintainer of weblate and openshift were helping us.
        </p>
      </MyNotes>
    </Slide>
  )
);
