import React from 'react';
import CodeSlide from 'spectacle-code-slide';

export const IntlSolution = React.memo(
  ({ slideIndex }: { slideIndex?: number }) => (
    <CodeSlide
      slideIndex={slideIndex}
      transition={[]}
      lang="jsx"
      code={
        /* eslint-disable-next-line @typescript-eslint/no-var-requires */
        require('./03-IntlSolution/code.raw').default
      }
      ranges={[
        {
          loc: [0, 1],
          title: 'Import IntlProvider',
          notes:
            'We start by installing and importing react-intl. We also considered react-i18next and other libs, but by the time, react-intl really had the best support for ICU.',
        },
        {
          loc: [12, 18],
          title: 'Add IntlProvider',
          notes: 'We add IntlProvider below our LocaleProvider.',
        },
        {
          loc: [29, 32],
          title: 'Use FormattedMessage',
          notes:
            'And we use FormattedMessage instead of our own Message we previously implemented.',
        },
      ]}
    />
  )
);
