import { NativeIntlSupport } from './00-NativeIntlSupport';
import { IntlSlide } from './01-Intl';
import { IntlSolutionTitle } from './02-IntlSolutionTitle';
import { IntlSolution } from './03-IntlSolution';
import { IntlSolutionFormatters } from './04-IntlSolutionFormatters';
import { IntlSolutionDX } from './05-IntlSolutionDX';
import { IntlSolutionTX } from './06-IntlSolutionTX';

export default [
  NativeIntlSupport,
  IntlSlide,
  IntlSolutionTitle,
  IntlSolution,
  IntlSolutionFormatters,
  IntlSolutionDX,
  IntlSolutionTX,
];
