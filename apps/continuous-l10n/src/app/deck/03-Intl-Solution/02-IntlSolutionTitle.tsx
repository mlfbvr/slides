import React from 'react';
import { MyNotes, TitleSlide } from '@mlfbvr/deck-components';

export const IntlSolutionTitle = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <TitleSlide title="Adding ICU support in our solution" hash={slideIndex}>
      <MyNotes>
        So that was quite a breakthrough, how can we use it in our solution and
        make billions with our great app?
      </MyNotes>
    </TitleSlide>
  )
);
