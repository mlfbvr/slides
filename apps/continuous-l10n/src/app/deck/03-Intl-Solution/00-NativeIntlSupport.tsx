import React from 'react';
import { MyNotes, TitleSlide } from '@mlfbvr/deck-components';

export const NativeIntlSupport = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <TitleSlide title="Native i18n support in JavaScript" hash={slideIndex}>
      <MyNotes>
        <p>So that was quite nice, but in JS?</p>
      </MyNotes>
    </TitleSlide>
  )
);
