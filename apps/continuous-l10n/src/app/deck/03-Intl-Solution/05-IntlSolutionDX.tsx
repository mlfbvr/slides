import React from 'react';
import { ConclusionTableSlide, MyNotes } from '@mlfbvr/deck-components';
import { DX } from '../../constants';

export const IntlSolutionDX = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <ConclusionTableSlide
      title="Developers"
      criterias={DX.INTL}
      previous={DX.NAIVE}
      hash={slideIndex}
    >
      <MyNotes>
        <p>
          So in terms of Developer Experience, the implementation difficulty
          wasn't really all that difficult. It was only necessary to install
          react-intl, use its provider and format components.
        </p>
        <p>
          We haven't really impacted the scalability of our solution nor the
          ease of integrating translations.
        </p>
        <p>
          In the other hand, we can easily handle complex messages through the
          ICU message syntax.
        </p>
      </MyNotes>
    </ConclusionTableSlide>
  )
);
