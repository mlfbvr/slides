import React from 'react';
import CodeSlide from 'spectacle-code-slide';

export const IntlSlide = React.memo(
  ({ slideIndex }: { slideIndex?: number }) => (
    <CodeSlide
      slideIndex={slideIndex}
      transition={[]}
      lang="jsx"
      code={
        /* eslint-disable-next-line @typescript-eslint/no-var-requires */
        require('./01-Intl/code.raw').default
      }
      ranges={[
        {
          loc: [0, 7],
          title: 'Format numbers',
          notes:
            'You can use formatters from the built-in Intl object. For example, here with NumberFormat',
        },
        {
          loc: [10, 19],
          title: 'Format dates',
          notes: 'Here with DateTimeFormat',
        },
        {
          loc: [22, 31],
          title: 'Get plural forms',
          notes: 'You can select the plural form of a number with PluralRules',
        },
        {
          loc: [34, 45],
          title: 'Get plural forms',
          notes:
            'It is obviously localized, so it helps us with the "I dont want that in my codebase" predicate',
        },
        {
          loc: [48, 55],
          title: 'Get ordinal forms',
          notes: 'Ordinal forms are found under the PluralRules formatter',
        },
        {
          loc: [58, 65],
          title: 'Get ordinal forms',
          notes: 'And it is thankfully also localized.',
        },
      ]}
    />
  )
);
