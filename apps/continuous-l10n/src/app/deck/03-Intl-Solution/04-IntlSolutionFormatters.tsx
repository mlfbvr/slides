import React from 'react';
import CodeSlide from 'spectacle-code-slide';

export const IntlSolutionFormatters = React.memo(
  ({ slideIndex }: { slideIndex?: number }) => (
    <CodeSlide
      slideIndex={slideIndex}
      transition={[]}
      lang="jsx"
      code={
        /* eslint-disable-next-line @typescript-eslint/no-var-requires */
        require('./04-IntlSolutionFormatters/code.raw').default
      }
      ranges={[
        {
          loc: [2, 6],
          title: 'Plural message',
          notes: 'We can now define plural forms in our translations',
        },
        {
          loc: [11, 12],
          title: 'Pass values',
          notes:
            'We only need to pass the values when calling FormattedMessage',
        },
        {
          loc: [17, 24],
          title: 'Format numbers',
          notes:
            'We can use FormattedNumber to format numbers more quickly than to define a formatter in a string',
        },
        {
          loc: [27, 39],
          title: 'Format dates',
          notes: 'Same thing with dates.',
        },
        {
          loc: [42, 48],
          title: 'Rich Text Formatting',
          notes:
            'And we can also do some rich text formatting. First, we define chunks or placeholders in our code. For example, I dont want the url of our shop to be in my translations files.',
        },
        {
          loc: [52, 56],
          title: 'Rich Text Formatting',
          notes: 'I can still pass other values.',
        },
        {
          loc: [56, 64],
          title: 'Rich Text Formatting',
          notes:
            'And I can replace the chunks with tags. It could be used in react-native with native components too!' +
            ' So here I tell react-intl to replace the part surrounded by a "a" tag in the translated message by a "a" tag with secured attributes. ' +
            'Since it is JSX, I could also use components here.',
        },
      ]}
    />
  )
);
