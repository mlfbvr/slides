import React from 'react';
import { ConclusionTableSlide, MyNotes } from '@mlfbvr/deck-components';
import { TX } from '../../constants';

export const IntlSolutionTX = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <ConclusionTableSlide
      title="Translators"
      criterias={TX.INTL}
      previous={TX.NAIVE}
      hash={slideIndex}
    >
      <MyNotes>
        <p>For translators, we still use JSON or excel files.</p>
        <p>
          On the other hand, the translators we met knew the ICU message syntax,
          you may still need to learn it.
        </p>
        <p>
          But, without any changes in code (you only need to pass values), you
          can support plural forms, pronouns with select, etc...
        </p>
      </MyNotes>
    </ConclusionTableSlide>
  )
);
