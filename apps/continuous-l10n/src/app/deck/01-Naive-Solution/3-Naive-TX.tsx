import React from 'react';
import { ConclusionTableSlide, MyNotes } from '@mlfbvr/deck-components';
import { TX } from '../../constants';

export const NaiveTX = React.memo(({ slideIndex }: { slideIndex: number }) => (
  <ConclusionTableSlide
    hash={slideIndex}
    title="Translators"
    criterias={TX.NAIVE}
  >
    <MyNotes>
      <p>But it wasn't all that fun for the translators too.</p>
      <p>
        If we sent them the JSON files, they had to know this format. Or we had
        to find a common ground with excel files.
      </p>
      <p>
        There wasn't any message syntax convention involved, meaning we couldn't
        support dates, plurals, etc...
      </p>
    </MyNotes>
  </ConclusionTableSlide>
));
