import React from 'react';
import CodeSlide from 'spectacle-code-slide';

export const NaiveSolutionImpl = React.memo(
  ({ slideIndex }: { slideIndex?: number }) => (
    <CodeSlide
      slideIndex={slideIndex}
      hash={slideIndex}
      transition={[]}
      lang="jsx"
      code={
        /* eslint-disable-next-line @typescript-eslint/no-var-requires */
        require('./naive-solution-impl.raw').default
      }
      ranges={[
        {
          loc: [2, 16],
          title: 'Define langs',
          notes: 'I start by defining a translation dictionary',
        },
        {
          loc: [19, 25],
          title: 'Define LocaleContext',
          notes:
            'I then create a context holding the current locale value and a function allowing to update its value.',
        },
        {
          loc: [28, 41],
          title: 'Define LocaleProvider',
          notes:
            'I now define a ContextProvider as you would by reading the context documentation',
        },
        {
          loc: [44, 53],
          title: 'Define a consumer',
          notes:
            'Then a first consumer which reads the current locale from the context and the translation dictionary to display the translated message',
        },
        {
          loc: [56, 73],
          title: 'Select your locale',
          notes:
            'And another consumer which allows me to select another locale and update the context stored value',
        },
        {
          loc: [76, 83],
          title: 'Put it all together',
          notes:
            "So, it's now noon, I have written my tests, opened my Pull Request, it was approved, merged, tested and deployed. Am I done ? Let's see.",
        },
      ]}
    />
  )
);
