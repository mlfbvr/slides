import { FirstStep } from './0-First-Step';
import { NaiveSolutionImpl } from './1-Naive-Solution-Impl';
import { NaiveDX } from './2-Naive-DX';
import { NaiveTX } from './3-Naive-TX';

export default [FirstStep, NaiveSolutionImpl, NaiveDX, NaiveTX];
