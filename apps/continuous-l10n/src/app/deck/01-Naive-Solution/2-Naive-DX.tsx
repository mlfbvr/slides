import React from 'react';
import { ConclusionTableSlide, MyNotes } from '@mlfbvr/deck-components';
import { DX } from '../../constants';

export const NaiveDX = React.memo(({ slideIndex }: { slideIndex: number }) => (
  <ConclusionTableSlide
    hash={slideIndex}
    title="Developers"
    criterias={DX.NAIVE}
  >
    <MyNotes>
      <div style={{ fontSize: '1.2rem' }}>
        <p>
          As for the Developer Experience (or DX), the implementation difficulty
          was really low. In a few hours it was fully implemented and deployed.
        </p>
        <p>
          But if I need to add a new language, I have to update the translations
          dictionary by hand.
        </p>
        <p>
          If I need to update translations, or renaming a translation key, or
          removing a key, I need to keep all dictionaries up-to-date by hand.
        </p>
        <p>
          For the little story, after 3 months of project, with only 4
          developers & 2 languages, it took one of us 2 days by sprint to make
          sure the translations dictionaries were up-to-date. To be honest,
          maybe the dullness of the task wasn't helping with efficiency.
        </p>
        <p>We'll see in the next User Story about "complex messages".</p>
        <p>
          And then, since we worked with translators, put your hands ups if this
          sounds familiar to you:
        </p>
        <p>
          We had to convert our translations files to Excel files, send them by
          mail to the translators, wait a few days, get the translations back,
          convert them back from excel.
        </p>
        <p>
          Yeah, it wasn't all that fun, and we loved the few days of lag,
          meaning the translations we got back were already outdated.
        </p>
      </div>
    </MyNotes>
  </ConclusionTableSlide>
));
