import React from 'react';
import { MyNotes, TitleSlide } from '@mlfbvr/deck-components';

export const FirstStep = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <TitleSlide
      hash={slideIndex}
      title="A first step towards Continuous Localization"
    >
      <MyNotes>
        <p>
          So let's start. We are monday morning, 9AM, and I pick the "implement
          i18n" user story in the backlog.
        </p>
      </MyNotes>
    </TitleSlide>
  )
);
