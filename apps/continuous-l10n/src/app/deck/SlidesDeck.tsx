import React from 'react';
import { Deck } from 'spectacle';
import Intro from './00-Intro';
import NaiveSolution from './01-Naive-Solution';
import ICU from './02-ICU';
import IntlSolution from './03-Intl-Solution';
import Gettext from './04-Gettext';
import SyncSolution from './05-SyncSolution';
import ContinuousLocalization from './06-ContinuousLocalization';
import Conclusion from './07-Conclusion';

import { mlfbvrTheme } from '@mlfbvr/spectacle-theme-mlfbvr';

const SECTIONS = [
  { name: 'intro', slides: Intro },
  { name: 'naive-solution', slides: NaiveSolution },
  { name: 'icu', slides: ICU },
  { name: 'intl-solution', slides: IntlSolution },
  { name: 'gettext', slides: Gettext },
  { name: 'sync-solution', slides: SyncSolution },
  { name: 'continuous-localization', slides: ContinuousLocalization },
  { name: 'conclusion', slides: Conclusion },
];

const renderSlide = (name) => (Component, index) =>
  <Component key={`${name}-${index}`} />;

const renderSection = ({ name, slides }) => slides.map(renderSlide(name));

const SlidesDeck = () => (
  <Deck
    theme={mlfbvrTheme}
    showFullscreenControl={false}
    controls={false}
    progress="none"
  >
    {SECTIONS.map(renderSection)}
  </Deck>
);

export default React.memo(SlidesDeck);
