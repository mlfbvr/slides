import React from 'react';
import { Slide } from 'spectacle';

import severalMonthsLater from '../../../assets/several-months-later.jpg';
import { MyNotes } from '@mlfbvr/deck-components';

export const SeveralMonthsLater = ({ slideIndex }: { slideIndex: number }) => (
  <Slide hash={slideIndex} bgImage={severalMonthsLater}>
    <MyNotes>
      <p>Speaking of plurals...</p>
      <p>Several months later...</p>
    </MyNotes>
  </Slide>
);
