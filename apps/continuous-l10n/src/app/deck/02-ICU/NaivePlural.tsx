import React from 'react';
import CodeSlide from 'spectacle-code-slide';

export const NaivePlural = React.memo(
  ({ slideIndex }: { slideIndex?: number }) => (
    <CodeSlide
      slideIndex={slideIndex}
      transition={[]}
      lang="jsx"
      code={
        /* eslint-disable-next-line @typescript-eslint/no-var-requires */
        require('./naive-plural.raw').default
      }
      ranges={[
        {
          loc: [2, 3],
          title: 'Is plural',
          notes:
            "Let's start with a isPlural function, returning true if count is anything but 1",
        },
        {
          loc: [6, 10],
          title: 'Find the right key',
          notes: 'Then find a key suffixed with dot singular or dot plural',
        },
        {
          loc: [13, 17],
          title: 'Or pluralize a word',
          notes:
            'Or my favorite, pluralize a word by appending a s to it. Put your hands up if you can see the obvious mistake here. Luckily, this company was not selling horses in France. In French: un cheval, des chevaux.',
        },
      ]}
    />
  )
);
