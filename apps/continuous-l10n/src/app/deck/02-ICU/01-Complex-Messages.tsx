import React from 'react';
import { Heading, Slide, List, ListItem, Text } from 'spectacle';

export const ComplexMessages = React.memo(() => (
  <Slide bgColor="primary">
    <Heading size={3} textColor="tertiary">
      Message syntax
    </Heading>

    <Text margin="4rem 0 0 0">Complex messages?</Text>

    <List margin="2rem auto" style={{ display: 'inline-block' }}>
      <ListItem>Variables</ListItem>
      <ListItem>Genders</ListItem>
      <ListItem>Plurals</ListItem>
      <ListItem>etc.</ListItem>
    </List>
  </Slide>
));
