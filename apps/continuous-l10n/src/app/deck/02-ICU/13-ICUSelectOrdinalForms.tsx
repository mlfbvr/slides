import React from 'react';
import { Code, Heading, Link, List, ListItem, Slide, Text } from 'spectacle';
import { LINKS } from '../../links';
import { MyNotes } from '@mlfbvr/deck-components';

export const ICUSelectOrdinalForms = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide bgColor="primary" hash={slideIndex}>
      <Heading size={3} textColor="tertiary" margin="0 0 2rem">
        selectordinal
      </Heading>

      <List margin="4rem 0 0" style={{ display: 'inline-block' }}>
        <ListItem>
          Keywords are the same as in <Code>plural</Code>.
        </ListItem>
        <ListItem>They do not have the same meaning.</ListItem>
      </List>

      <Text margin="4rem 0 0">
        → Keywords are (still) <i>locale specific</i>.
      </Text>

      <Text margin="2rem 0 0">
        → See the{' '}
        <Link target="_blank" href={LINKS.CLDR_PLURAL}>
          Unicode CLDR
        </Link>
        .
      </Text>
      <MyNotes>
        <p>As I said, keywords are the same as in the plural type.</p>
        <p>But you should know that they don't follow the same rules.</p>
        <p>
          They are still localized and you can find the rules in the Unicode
          CLDR I've linked at the bottom of the slide.
        </p>
      </MyNotes>
    </Slide>
  )
);
