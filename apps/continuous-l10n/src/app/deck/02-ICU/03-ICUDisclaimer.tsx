import React from 'react';
import { Heading, Slide, Text } from 'spectacle';
import { MyNotes } from '@mlfbvr/deck-components';

export const ICUDisclaimer = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide bgColor="primary" hash={slideIndex}>
      <Heading size={3} textColor="tertiary">
        ICU: Disclaimer
      </Heading>

      <Text margin="4rem 0 2rem">→ Only in C/C++ and Java.</Text>
      <Text margin="2rem 0">→ But implemented in a lot of languages.</Text>

      <MyNotes>
        <p>A little disclaimer about the ICU library</p>
        <p>It is officially only implemented in C/C++ and Java</p>
        <p>
          But thankfully, the implementation was ported in a lot of languages,
          including, you could guess, JS
        </p>
      </MyNotes>
    </Slide>
  )
);
