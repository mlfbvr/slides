import React from 'react';
import { Heading, List, ListItem, Slide } from 'spectacle';
import { CodeDark, MyNotes } from '@mlfbvr/deck-components';

export const ICUFormatters = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide bgColor="primary" hash={slideIndex}>
      <Heading size={3} textColor="tertiary" margin="0 0 2rem">
        Formatters
      </Heading>

      <CodeDark>
        {
          /* eslint-disable-next-line @typescript-eslint/no-var-requires */
          require('./04-ICUFormatters/formatter.raw.icu').default
        }
      </CodeDark>

      <List>
        <ListItem margin="1rem 0">
          <i>
            <b>input</b>
          </i>{' '}
          input data.
        </ListItem>
        <ListItem margin="1rem 0">
          <i>
            <b>type</b>
          </i>{' '}
          (optional) <i>number</i>, <i>date</i>, <i>time</i>, <i>select</i>,{' '}
          <i>plural</i>, <i>selectordinal</i>.
        </ListItem>
        <ListItem margin="1rem 0">
          <i>
            <b>format</b>
          </i>{' '}
          (optional) how to render the <i>input</i> based on <i>type</i>.
        </ListItem>
      </List>

      <CodeDark>
        {
          /* eslint-disable-next-line @typescript-eslint/no-var-requires */
          require('./04-ICUFormatters/example.raw.icu').default
        }
      </CodeDark>

      <MyNotes>
        <div style={{ fontSize: '1.5rem' }}>
          <p>A formatter, defined in curly braces, have 3 parts.</p>
          <p>
            The first one is mandatory and is the name of the input data, like
            in other template languages. AngularJS developers will be familiar
            with this syntax for example.
          </p>
          <p>
            The second part allows the translator to select the type of the
            input data. For example, number or date.
          </p>
          <p>
            The last part allows the translator to be more precise on the
            formatted output. For example, for a specified number type, we could
            precise the format to be percent.
          </p>
          <p>
            I am going to show you a few formatters quite quickly, the
            documentation and the slides will help you more when implementing
            it.
          </p>
        </div>
      </MyNotes>
    </Slide>
  )
);
