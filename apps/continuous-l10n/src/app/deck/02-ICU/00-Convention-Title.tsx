import React from 'react';
import { TitleSlide } from '@mlfbvr/deck-components';

export const ConventionTitle = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <TitleSlide hash={slideIndex} title="Message syntax convention" />
  )
);
