import React from 'react';
import { Heading, Slide, Text } from 'spectacle';
import { CodeDark, MyNotes } from '@mlfbvr/deck-components';

import classNames from './12-ICUSelectOrdinals/slide.module.scss';

export const ICUSelectOrdinals = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide bgColor="primary" hash={slideIndex}>
      <Heading size={3} textColor="tertiary" margin="0 0 2rem">
        selectordinal
      </Heading>

      <div className={classNames.pluralsContainer}>
        <div>
          <Text textAlign="left">en</Text>
          <CodeDark
            className={classNames.codeContainer}
            preClassName={classNames.pre}
            codeClassName={classNames.code}
          >
            {/* eslint-disable-next-line @typescript-eslint/no-var-requires */}
            {require('./12-ICUSelectOrdinals/selectordinal.en.raw.icu').default}
          </CodeDark>
        </div>

        <div>
          <Text textAlign="left">fr</Text>
          <CodeDark
            className={classNames.codeContainer}
            preClassName={classNames.pre}
            codeClassName={classNames.code}
          >
            {/* eslint-disable-next-line @typescript-eslint/no-var-requires */}
            {require('./12-ICUSelectOrdinals/selectordinal.fr.raw.icu').default}
          </CodeDark>
        </div>
      </div>
      <MyNotes>
        <p>
          For ordinals, like "first", "second", etc..., you can use the
          selectordinal type.
        </p>
        <p>
          You find keywords similar than those of the plural types, zero, one,
          etc...
        </p>
        <p>
          The hash symbol, which I haven't used in my examples before, tells the
          formatter engine to reuse the value of the input.
        </p>
        <p>Here for one, it will output one followed by s and t.</p>
      </MyNotes>
    </Slide>
  )
);
