import React from 'react';
import { Heading, Slide, Text } from 'spectacle';

import classNames from './02-HelloICU/slide.module.scss';
import { CodeDark, MyNotes } from '@mlfbvr/deck-components';

export const HelloICU = React.memo(({ slideIndex }: { slideIndex: number }) => (
  <Slide bgColor="primary" hash={slideIndex}>
    <Heading size={3} textColor="tertiary">
      ICU message syntax
    </Heading>

    <Text margin="4rem 0 2rem">Defined in the ICU library</Text>

    <div className={classNames.codeContainer}>
      {/* eslint-disable-next-line @typescript-eslint/no-var-requires */}
      <CodeDark>{require('./02-HelloICU/hello.en.raw.icu').default}</CodeDark>
      {/* eslint-disable-next-line @typescript-eslint/no-var-requires */}
      <CodeDark>{require('./02-HelloICU/hello.fr.raw.icu').default}</CodeDark>
      {/* eslint-disable-next-line @typescript-eslint/no-var-requires */}
      <CodeDark>{require('./02-HelloICU/hello.ar.raw.icu').default}</CodeDark>
    </div>

    <MyNotes>
      <p>
        It's called ICU, short for International Components for Unicode and it
        defines a message format for internationalization
      </p>
      <p>
        You define formatters inside curly brackets, which could be used by the
        ICU library to format the string.
      </p>
    </MyNotes>
  </Slide>
));
