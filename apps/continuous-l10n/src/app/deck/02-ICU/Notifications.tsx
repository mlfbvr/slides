import React from 'react';
import { Code, Heading, List, ListItem, Slide } from 'spectacle';
import { MyNotes } from '@mlfbvr/deck-components';

export const Notifications = ({ slideIndex }: { slideIndex: number }) => (
  <Slide bgColor="primary" hash={slideIndex}>
    <Heading size={3} textColor="tertiary" margin="0 0 2rem">
      US: Display notifications count
    </Heading>

    <List margin="4rem 0 0">
      <ListItem margin="1rem 0 0">
        <Code>"You have no pending notifications"</Code>
      </ListItem>
      <ListItem margin="1rem 0 0">
        <Code>"You have a pending notification"</Code>
      </ListItem>
      <ListItem margin="1rem 0 0">
        <Code>"You have N pending notifications"</Code>
      </ListItem>
    </List>
    <MyNotes>
      <p>A new User Story finds its way in the backlog.</p>
      <p>
        We need to display a message to the user depending of its notifications
        count.
      </p>
      <p>So, let's take this US and implement it.</p>
      <p>Please be attentive and find all the mistakes I am going to make.</p>
      <p>Disclaimer: this is an implementation I have seen in real life.</p>
    </MyNotes>
  </Slide>
);
