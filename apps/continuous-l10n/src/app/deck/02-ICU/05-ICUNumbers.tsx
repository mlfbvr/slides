import React from 'react';
import { Heading, Slide, Text } from 'spectacle';

import classNames from './05-ICUNumbers/slide.module.scss';

import { CodeDark, MyNotes } from '@mlfbvr/deck-components';

export const ICUNumbers = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide bgColor="primary" hash={slideIndex}>
      <Heading size={3} textColor="tertiary" margin="0 0 2rem">
        number
      </Heading>

      <div className={classNames.codeContainer}>
        {/* eslint-disable-next-line @typescript-eslint/no-var-requires */}
        <CodeDark codeClassName={classNames.code}>
          {require('./05-ICUNumbers/number.raw.icu').default}
        </CodeDark>

        <div>
          <Text textAlign="left">fr</Text>
          {/* eslint-disable-next-line @typescript-eslint/no-var-requires */}
          <CodeDark codeClassName={classNames.code}>
            {require('./05-ICUNumbers/result.fr.raw.icu').default}
          </CodeDark>
        </div>

        <div>
          <Text textAlign="left">en</Text>
          {/* eslint-disable-next-line @typescript-eslint/no-var-requires */}
          <CodeDark codeClassName={classNames.code}>
            {require('./05-ICUNumbers/result.en.raw.icu').default}
          </CodeDark>
        </div>
      </div>

      <Text margin="4rem 0 0 0">
        By default, the input type is not inferred.
      </Text>
      <MyNotes>
        <p>As you can see here, the input type was not inferred.</p>
        <p>
          If I don't specify the second part to be number, the count will be
          displayed as it is.
        </p>
        <p>
          If I specify the type however, thousands and decimal separators will
          be localized.
        </p>
        <p>
          I could also specify a percent format part, but beware, it will
          multiply the input by 100 too. So for example, to display 50%, count
          should be 0.5.
        </p>
      </MyNotes>
    </Slide>
  )
);
