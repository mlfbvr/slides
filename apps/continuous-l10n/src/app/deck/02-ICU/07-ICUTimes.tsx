import React from 'react';
import { Heading, Slide, Text } from 'spectacle';
import { CodeDark, MyNotes } from '@mlfbvr/deck-components';

import classNames from './07-ICUTimes/slide.module.scss';

export const ICUTimes = React.memo(({ slideIndex }: { slideIndex: number }) => (
  <Slide bgColor="primary" hash={slideIndex}>
    <Heading size={3} textColor="tertiary" margin="0 0 2rem">
      time
    </Heading>

    <div className={classNames.codeContainer}>
      {/* eslint-disable-next-line @typescript-eslint/no-var-requires */}
      <CodeDark codeClassName={classNames.code}>
        {require('./07-ICUTimes/time.raw.icu').default}
      </CodeDark>

      <div>
        <Text textAlign="left">fr-FR</Text>
        {/* eslint-disable-next-line @typescript-eslint/no-var-requires */}
        <CodeDark codeClassName={classNames.code}>
          {require('./07-ICUTimes/result.fr.raw.icu').default}
        </CodeDark>
      </div>

      <div>
        <Text textAlign="left">en-US</Text>
        {/* eslint-disable-next-line @typescript-eslint/no-var-requires */}
        <CodeDark codeClassName={classNames.code}>
          {require('./07-ICUTimes/result.en.raw.icu').default}
        </CodeDark>
      </div>
    </div>
    <MyNotes>
      <p>Same thing for times</p>
    </MyNotes>
  </Slide>
));
