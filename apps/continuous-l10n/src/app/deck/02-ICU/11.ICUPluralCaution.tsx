import React from 'react';
import { Code, Heading, Link, List, ListItem, Slide, Text } from 'spectacle';
import { LINKS } from '../../links';
import { MyNotes } from '@mlfbvr/deck-components';

export const ICUPluralCaution = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide bgColor="primary" hash={slideIndex}>
      <Heading size={3} textColor="tertiary" margin="0 0 2rem">
        plural
      </Heading>

      <List margin="4rem 0 0" style={{ display: 'inline-block' }}>
        <ListItem>
          <Code>zero</Code> is not the same as <Code>=0</Code>.
        </ListItem>
        <ListItem style={{ marginTop: '1rem' }}>
          <Code>one</Code> is not the same as <Code>=1</Code>.
        </ListItem>
        <ListItem style={{ marginTop: '1rem' }}>etc.</ListItem>
      </List>

      <Text margin="4rem 0 0">
        → Keywords are <i>locale specific</i>.
      </Text>

      <Text margin="2rem 0 0">
        → See the{' '}
        <Link target="_blank" href={LINKS.CLDR_PLURAL}>
          Unicode CLDR
        </Link>
        .
      </Text>
      <MyNotes>
        <p>
          The plural form zero and one are not the same as equals zero and
          equals one
        </p>
        <p>
          For example, in french, the plural form "one" include all numbers
          lesser than 2!
        </p>
        <p>
          I've put here a link to the Unicode CLDR which explains for all
          languages the plural form rules.
        </p>
      </MyNotes>
    </Slide>
  )
);
