import React from 'react';
import { Heading, Slide, Text } from 'spectacle';

import classNames from './08-ICUSelects/slide.module.scss';
import { CodeDark, MyNotes } from '@mlfbvr/deck-components';

export const ICUSelects = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide bgColor="primary" hash={slideIndex}>
      <Heading size={3} textColor="tertiary" margin="0 0 2rem">
        select
      </Heading>

      <Text margin="2rem 0">
        <i>
          <b>select</b>
        </i>{' '}
        works like a <i>switch</i>
      </Text>

      <div className={classNames.resultsContainer}>
        <div>
          <Text textAlign="left">en</Text>
          {/* eslint-disable-next-line @typescript-eslint/no-var-requires */}
          <CodeDark codeClassName={classNames.code}>
            {require('./08-ICUSelects/select.en.raw.icu').default}
          </CodeDark>
        </div>

        <div>
          <Text textAlign="left">fr</Text>
          {/* eslint-disable-next-line @typescript-eslint/no-var-requires */}
          <CodeDark codeClassName={classNames.code}>
            {require('./08-ICUSelects/select.fr.raw.icu').default}
          </CodeDark>
        </div>
      </div>
      <MyNotes>
        <p>For select, we have a new kind of syntax.</p>
        <p>
          Select works like a switch, and, depending on the value of the input,
          we can select a different output, specified in another curly braces
          block.
        </p>
        <p>
          Depending on the language, we could refactor the whole sentence or
          just a part of it, like it is shown here.
        </p>
        <p>
          Note that the default case is called "other", like the "other" plural
          form I've shown earlier.
        </p>
        <p>Speaking of plural</p>
      </MyNotes>
    </Slide>
  )
);
