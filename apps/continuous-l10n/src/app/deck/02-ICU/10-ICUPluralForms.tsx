import React from 'react';
import { Code, Heading, List, ListItem, Slide, Text } from 'spectacle';
import { MyNotes } from '@mlfbvr/deck-components';

export const ICUPluralForms = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide bgColor="primary" hash={slideIndex}>
      <Heading size={3} textColor="tertiary" margin="0 0 4rem">
        plural
      </Heading>

      <Text>
        Plural <i>format</i> string keywords:
      </Text>
      <List style={{ display: 'inline-block' }}>
        <ListItem>
          <Code>zero</Code>{' '}
        </ListItem>
        <ListItem style={{ marginTop: '1rem' }}>
          <Code>one</Code>
        </ListItem>
        <ListItem style={{ marginTop: '1rem' }}>
          <Code>two</Code>
        </ListItem>
        <ListItem style={{ marginTop: '1rem' }}>
          <Code>few</Code>
        </ListItem>
        <ListItem style={{ marginTop: '1rem' }}>
          <Code>many</Code>
        </ListItem>
        <ListItem style={{ marginTop: '1rem' }}>
          <Code>other</Code>
        </ListItem>
        <ListItem style={{ marginTop: '1rem' }}>
          <Code>=NB</Code> (e.g. <Code>=0</Code>, <Code>=1</Code>, etc.)
        </ListItem>
      </List>
      <MyNotes>
        <p>
          So here are the different plural forms: zero, one, two, few, many
          other.
        </p>
        <p>
          You could also make special cases for exact values with "equals
          value".
        </p>
        <p>But beware!</p>
      </MyNotes>
    </Slide>
  )
);
