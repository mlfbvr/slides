import { ConventionTitle } from './00-Convention-Title';

import PluralRulesInterlude from './00-PluralRules';
import { HelloICU } from './02-HelloICU';
import { ICUDisclaimer } from './03-ICUDisclaimer';
import { ICUFormatters } from './04-ICUFormatters';
import { ICUNumbers } from './05-ICUNumbers';
import { ICUDates } from './06-ICUDates';
import { ICUTimes } from './07-ICUTimes';
import { ICUSelects } from './08-ICUSelects';
import { ICUPlurals } from './09-ICUPlurals';
import { ICUPluralForms } from './10-ICUPluralForms';
import { ICUPluralCaution } from './11.ICUPluralCaution';
import { ICUSelectOrdinals } from './12-ICUSelectOrdinals';
import { ICUSelectOrdinalForms } from './13-ICUSelectOrdinalForms';
import { SeveralMonthsLater } from './SeveralMonthsLater';
import { Notifications } from './Notifications';
import { NaivePlural } from './NaivePlural';

export default [
  // ComplexMessages,
  SeveralMonthsLater,
  Notifications,
  NaivePlural,
  ...PluralRulesInterlude,
  ConventionTitle,
  HelloICU,
  ICUDisclaimer,
  ICUFormatters,
  ICUNumbers,
  ICUDates,
  ICUTimes,
  ICUSelects,
  ICUPlurals,
  ICUPluralForms,
  ICUPluralCaution,
  ICUSelectOrdinals,
  ICUSelectOrdinalForms,
];
