import React from 'react';
import { Heading, Slide, Text } from 'spectacle';

import classNames from './06-ICUDates/slide.module.scss';
import { CodeDark, MyNotes } from '@mlfbvr/deck-components';

export const ICUDates = React.memo(({ slideIndex }: { slideIndex: number }) => (
  <Slide bgColor="primary" hash={slideIndex}>
    <Heading size={3} textColor="tertiary" margin="0 0 2rem">
      date
    </Heading>

    <div className={classNames.codeContainer}>
      {/* eslint-disable-next-line @typescript-eslint/no-var-requires */}
      <CodeDark codeClassName={classNames.code}>
        {require('./06-ICUDates/date.raw.icu').default}
      </CodeDark>

      <div>
        <Text textAlign="left">fr-FR</Text>
        {/* eslint-disable-next-line @typescript-eslint/no-var-requires */}
        <CodeDark codeClassName={classNames.code}>
          {require('./06-ICUDates/result.fr.raw.icu').default}
        </CodeDark>
      </div>

      <div>
        <Text textAlign="left">en-GB</Text>
        {/* eslint-disable-next-line @typescript-eslint/no-var-requires */}
        <CodeDark codeClassName={classNames.code}>
          {require('./06-ICUDates/result.en.raw.icu').default}
        </CodeDark>
      </div>
    </div>
    <MyNotes>
      <p>
        For dates, we can specify short, medium, long and full formats, just
        like with moment and its successors.
      </p>
      <p>Output will continue to be localized.</p>
    </MyNotes>
  </Slide>
));
