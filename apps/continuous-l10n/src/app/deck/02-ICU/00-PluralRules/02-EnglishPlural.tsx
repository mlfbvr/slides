import React from 'react';
import { Heading, Slide } from 'spectacle';
import { MyNotes, PluralRulesTable } from '@mlfbvr/deck-components';

export const EnglishPlural = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide hash={slideIndex} bgColor="primary">
      <Heading size={3} textColor="tertiary">
        English
      </Heading>

      <PluralRulesTable
        pluralRules={[
          { keyword: 'one', rule: 'n === 1' },
          { keyword: 'other', rule: 'rest' },
        ]}
      />
      <MyNotes>
        <p>
          In English, to be singular, the number needs to be exactly one. So 0,
          0.5 and 1.5 are all plural!
        </p>
        <p>
          The first time I realized that in English and in French we had
          different plural rules, I was quite shocked.
        </p>
        <p>Did you think I was over?</p>
      </MyNotes>
    </Slide>
  )
);
