import { PluralRules } from './00-PluralRules';
import { FrenchPlural } from './01-FrenchPlural';
import { EnglishPlural } from './02-EnglishPlural';
import { CzechPlural } from './03-CzechPlural';
import { ArabicPlural } from './04-ArabicPlural';
import { RussianPluralMeme } from './05-RussianPluralMeme';
import { RussianPlural } from './06-RussianPlural';
import { InterludeEnd } from './07-InterludeEnd';

export default [
  PluralRules,
  FrenchPlural,
  EnglishPlural,
  CzechPlural,
  ArabicPlural,
  RussianPluralMeme,
  RussianPlural,
  InterludeEnd,
];
