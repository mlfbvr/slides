import React from 'react';
import { Heading, Slide } from 'spectacle';
import { MyNotes, PluralRulesTable } from '@mlfbvr/deck-components';

export const CzechPlural = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide hash={slideIndex} bgColor="primary">
      <Heading size={3} textColor="tertiary">
        Czech
      </Heading>

      <PluralRulesTable
        pluralRules={[
          { keyword: 'one', rule: 'n === 1' },
          { keyword: 'few', rule: '[2, 3, 4].includes(n)' },
          { keyword: 'many', rule: 'n % 1 !== 0' },
          { keyword: 'other', rule: 'rest' },
        ]}
      />
      <MyNotes>
        <p>In Czech, we even have more plural forms!</p>
        <p>Singular is still 1 exactly</p>
        <p>But we also have a form called few for 2, 3 and 4</p>
        <p>Another one called many for all numbers that have a decimal part.</p>
        <p>And the rest which is... all the other integers.</p>
      </MyNotes>
    </Slide>
  )
);
