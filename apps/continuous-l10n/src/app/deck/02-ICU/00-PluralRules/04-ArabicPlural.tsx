import React from 'react';
import { Heading, Slide } from 'spectacle';
import { MyNotes, PluralRulesTable } from '@mlfbvr/deck-components';

export const ArabicPlural = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide hash={slideIndex} bgColor="primary">
      <Heading size={3} textColor="tertiary">
        Arabic
      </Heading>

      <PluralRulesTable
        pluralRules={[
          { keyword: 'zero', rule: 'n === 0' },
          { keyword: 'one', rule: 'n === 1' },
          { keyword: 'two', rule: 'n === 2' },
          { keyword: 'few', rule: '3 <= (n % 100) <= 10' },
          { keyword: 'many', rule: '11 <= (n % 100) <= 99' },
          { keyword: 'other', rule: 'rest' },
        ]}
      />

      <MyNotes>
        <p>In Arabic, we add two plural forms for zero and two!</p>
        <p>The few form is for all the numbers between 3 and 10 modulo 100</p>
        <p>The many form is for all the numbers between 11 and 99 modulo 100</p>
        <p>
          And the rest which are... all the numbers between 0 and 3 modulo 100.
        </p>
      </MyNotes>
    </Slide>
  )
);
