import React from 'react';
import { MyNotes, TitleSlide } from '@mlfbvr/deck-components';

export const PluralRules = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <TitleSlide hash={slideIndex} title="Interlude: plural rules">
      <MyNotes>
        <p>So. Let's take a little interlude and talk about plural rules.</p>
        <p>Again, a little show of hands.</p>
        <p>
          Did anyone find the mistake with the isPlural implementation before?
        </p>
      </MyNotes>
    </TitleSlide>
  )
);
