import React from 'react';
import { Heading, Image, Slide } from 'spectacle';

import tryNotToCry from '../../../../assets/try-not-to-cry.gif';

export const RussianPluralMeme = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide bgColor="primary" hash={slideIndex}>
      <Heading size={3} textColor="tertiary">
        Russian
      </Heading>

      <Image margin="4rem auto" width="16rem" src={tryNotToCry} />
    </Slide>
  )
);
