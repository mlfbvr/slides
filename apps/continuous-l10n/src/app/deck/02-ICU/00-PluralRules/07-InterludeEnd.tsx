import React from 'react';
import { TitleSlide } from '@mlfbvr/deck-components';

export const InterludeEnd = React.memo(({ slideIndex }: { slideIndex }) => (
  <TitleSlide hash={slideIndex} title="Back to message syntax" />
));
