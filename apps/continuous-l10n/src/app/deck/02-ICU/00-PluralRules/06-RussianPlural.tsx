import React from 'react';
import { Code, Heading, Slide } from 'spectacle';

import { MyNotes, PluralRulesTable } from '@mlfbvr/deck-components';

import classNames from './russian-plural.module.scss';

const Rule = (rule) => (
  <pre>
    <Code>
      {
        /* eslint-disable-next-line @typescript-eslint/no-var-requires */
        require(`./russian.${rule}.raw.icu`).default
      }
    </Code>
  </pre>
);

export const RussianPlural = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide
      bgColor="primary"
      className={classNames.russianSlide}
      hash={slideIndex}
    >
      <Heading size={3} textColor="tertiary">
        Russian
      </Heading>

      <PluralRulesTable
        pluralRules={[
          { keyword: 'one', rule: Rule('one'), preformatted: true },
          {
            keyword: 'few',
            rule: Rule('few'),
            preformatted: true,
          },
          {
            keyword: 'many',
            rule: Rule('many'),
            preformatted: true,
          },
          { keyword: 'other', rule: 'rest' },
        ]}
      />
      <MyNotes>
        <p>I think you guessed my point.</p>
        <p>Seeing this, what is the conclusion of this interlude?</p>
        <p>
          I don't want to see all these rules in my codebase. I don't want to
          write them, I don't want to test them, I don't want to maintain them.
          And in the darkness, bind them.
        </p>
      </MyNotes>
    </Slide>
  )
);
