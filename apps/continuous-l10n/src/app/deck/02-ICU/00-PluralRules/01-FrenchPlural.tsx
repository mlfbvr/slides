import React from 'react';
import { Heading, Slide } from 'spectacle';
import { MyNotes, PluralRulesTable } from '@mlfbvr/deck-components';

export const FrenchPlural = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide hash={slideIndex} bgColor="primary">
      <Heading size={3} textColor="tertiary">
        French
      </Heading>

      <PluralRulesTable
        pluralRules={[
          { keyword: 'one', rule: 'n < 2', example: '1,5 jours' },
          { keyword: 'other', rule: 'rest', example: '2 chevaux; 2,5 jours' },
        ]}
      />
      <MyNotes>
        <p>
          In French, Singular is any number lesser than 2. For example, 1.5 is
          singular!
        </p>
        <p>The rest is plural.</p>
      </MyNotes>
    </Slide>
  )
);
