import React from 'react';
import { Heading, Slide, Text } from 'spectacle';

import classNames from './09-ICUPlurals/slide.module.scss';
import { CodeDark, MyNotes } from '@mlfbvr/deck-components';

export const ICUPlurals = React.memo(
  ({ slideIndex }: { slideIndex: number }) => (
    <Slide
      bgColor="primary"
      hash={slideIndex}
      contentStyles={{
        maxWidth: 'none',
      }}
    >
      <Heading size={3} textColor="tertiary" margin="0 0 2rem">
        plural
      </Heading>

      <div className={classNames.pluralsContainer}>
        <div>
          <Text textAlign="left">en</Text>
          <CodeDark
            className={classNames.codeContainer}
            preClassName={classNames.pre}
            codeClassName={classNames.code}
          >
            {/* eslint-disable-next-line @typescript-eslint/no-var-requires */}
            {require('./09-ICUPlurals/plural.en.raw.icu').default}
          </CodeDark>
        </div>

        <div>
          <Text textAlign="left">fr</Text>
          <CodeDark
            className={classNames.codeContainer}
            preClassName={classNames.pre}
            codeClassName={classNames.code}
          >
            {/* eslint-disable-next-line @typescript-eslint/no-var-requires */}
            {require('./09-ICUPlurals/plural.fr.raw.icu').default}
          </CodeDark>
        </div>

        <div>
          <Text textAlign="left">cz</Text>
          <CodeDark
            className={classNames.codeContainer}
            preClassName={classNames.pre}
            codeClassName={classNames.code}
          >
            {/* eslint-disable-next-line @typescript-eslint/no-var-requires */}
            {require('./09-ICUPlurals/plural.cz.raw.icu').default}
          </CodeDark>
        </div>
      </div>
      <MyNotes>
        <p>Using plural feels like using select</p>
        <p>
          Instead of using input values, you specify plural forms before the
          curly brackets sub-blocks.
        </p>
        <p>
          This allows the developer to not care about multiple possible plural
          forms, only the translator for the given language.
        </p>
      </MyNotes>
    </Slide>
  )
);
