import React from 'react';
import SlidesDeck from './deck/SlidesDeck';

const App = () => <SlidesDeck />;

export default React.memo(App);
