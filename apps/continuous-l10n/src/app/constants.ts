export enum Label {
  IMPLEM = 'Implementation difficulty',
  NEW_LANG = 'Scalability to add a new language',
  UPDATE_LANG = 'Scalability to update translations',
  COMPLEX_MSG = 'Handle variables in messages',
  INTEGRATE_TRANSLATIONS = 'Integrate translations',
  TRANSLATION_DIFFICULTY = 'Translation difficulty',
  MSG_FORMAT = 'Message format',
}

export enum Difficulty {
  EASY = 'easy',
  MEDIUM = 'medium',
  HARD = 'hard',
}

export type Criteria = {
  label: Label;
  note: string;
  difficulty: Difficulty;
};

export const DX: Record<
  'NAIVE' | 'INTL' | 'SYNC_SOLUTION' | 'CONTINUOUS',
  Criteria[]
> = {
  NAIVE: [
    {
      label: Label.IMPLEM,
      note: 'Up-and-running very fast',
      difficulty: Difficulty.EASY,
    },
    {
      label: Label.NEW_LANG,
      note: 'By hand',
      difficulty: Difficulty.MEDIUM,
    },
    {
      label: Label.UPDATE_LANG,
      note: 'Keep translations up-to-date by hand',
      difficulty: Difficulty.HARD,
    },
    {
      label: Label.COMPLEX_MSG,
      note: 'Cannot handle complex messages',
      difficulty: Difficulty.HARD,
    },
    {
      label: Label.INTEGRATE_TRANSLATIONS,
      note: 'Need to merge translators changes by hand',
      difficulty: Difficulty.HARD,
    },
  ],
  INTL: [
    {
      label: Label.IMPLEM,
      note: 'Up-and-running very fast',
      difficulty: Difficulty.EASY,
    },
    {
      label: Label.NEW_LANG,
      note: 'By hand',
      difficulty: Difficulty.MEDIUM,
    },
    {
      label: Label.UPDATE_LANG,
      note: 'Keep translations up-to-date by hand',
      difficulty: Difficulty.HARD,
    },
    {
      label: Label.COMPLEX_MSG,
      note: 'ICU message syntax',
      difficulty: Difficulty.EASY,
    },
    {
      label: Label.INTEGRATE_TRANSLATIONS,
      note: 'Need to merge translators changes by hand',
      difficulty: Difficulty.HARD,
    },
  ],
  SYNC_SOLUTION: [
    {
      label: Label.IMPLEM,
      note: 'Need to bootstrap some scripts',
      difficulty: Difficulty.MEDIUM,
    },
    {
      label: Label.NEW_LANG,
      note: 'Single command',
      difficulty: Difficulty.EASY,
    },
    {
      label: Label.UPDATE_LANG,
      note: 'Single command for all languages',
      difficulty: Difficulty.EASY,
    },
    {
      label: Label.COMPLEX_MSG,
      note: 'ICU message syntax',
      difficulty: Difficulty.EASY,
    },
    {
      label: Label.INTEGRATE_TRANSLATIONS,
      note: 'Need to merge translators changes by hand',
      difficulty: Difficulty.HARD,
    },
  ],
  CONTINUOUS: [
    {
      label: Label.IMPLEM,
      note: 'Need to bootstrap some scripts',
      difficulty: Difficulty.MEDIUM,
    },
    {
      label: Label.NEW_LANG,
      note: 'Single command',
      difficulty: Difficulty.EASY,
    },
    {
      label: Label.UPDATE_LANG,
      note: 'Single command for all languages',
      difficulty: Difficulty.EASY,
    },
    {
      label: Label.COMPLEX_MSG,
      note: 'ICU message syntax',
      difficulty: Difficulty.EASY,
    },
    {
      label: Label.INTEGRATE_TRANSLATIONS,
      note: 'Translations follow versioning',
      difficulty: Difficulty.EASY,
    },
  ],
};

export const TX: Record<
  'NAIVE' | 'INTL' | 'SYNC_SOLUTION' | 'CONTINUOUS',
  Criteria[]
> = {
  NAIVE: [
    {
      label: Label.TRANSLATION_DIFFICULTY,
      note: 'Need to know JSON format',
      difficulty: Difficulty.MEDIUM,
    },
    {
      label: Label.MSG_FORMAT,
      note: 'No message syntax convention',
      difficulty: Difficulty.HARD,
    },
  ],
  INTL: [
    {
      label: Label.TRANSLATION_DIFFICULTY,
      note: 'Need to know JSON format',
      difficulty: Difficulty.MEDIUM,
    },
    {
      label: Label.MSG_FORMAT,
      note: 'Common message syntax, can be complex',
      difficulty: Difficulty.MEDIUM,
    },
  ],
  SYNC_SOLUTION: [
    {
      label: Label.TRANSLATION_DIFFICULTY,
      note: 'External tools to update PO files easily',
      difficulty: Difficulty.EASY,
    },
    {
      label: Label.MSG_FORMAT,
      note: 'Common message syntax, can be complex',
      difficulty: Difficulty.MEDIUM,
    },
  ],
  CONTINUOUS: [
    {
      label: Label.TRANSLATION_DIFFICULTY,
      note: 'Web application, no need to handle PO files',
      difficulty: Difficulty.EASY,
    },
    {
      label: Label.MSG_FORMAT,
      note: 'Common message syntax, can be complex',
      difficulty: Difficulty.MEDIUM,
    },
  ],
};
