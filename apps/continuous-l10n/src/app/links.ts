export const LINKS = {
  SELF: 'https://tinyurl.com/mlfbvr-continuous-l10n-readme',
  SLIDES: 'https://tinyurl.com/mlfbvr-continuous-l10n',
  CLDR_PLURAL:
    'https://unicode-org.github.io/cldr-staging/charts/latest/supplemental/language_plural_rules.html',
};
