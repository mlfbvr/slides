export default async () => {
  /*
   * Hack in order to make corejs work
   * Add global to window, assigning the value of window itself.
   */
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  (window as any).global = window;
};
