import { bootstrapPrism } from '@mlfbvr/prism';

bootstrapPrism(['jsx', 'bash', 'javascript', 'json']);
