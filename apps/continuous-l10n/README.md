# Continuous localization

## Useful links

- Community: [![Gitter](https://badges.gitter.im/mlfbvr-slides/community.svg)](https://gitter.im/mlfbvr-slides)
- Slides: [https://bit.ly/continuous-l10n](https://bit.ly/continuous-l10n)
- Self: [https://bit.ly/continuousl10n](https://bit.ly/continuousl10n)

## Plural

- [CLDR](https://unicode-org.github.io/cldr-staging/charts/latest/supplemental/language_plural_rules.html)
  - [Operands](http://unicode.org/reports/tr35/tr35-numbers.html#Operands)
  - [Plural rules](http://unicode.org/reports/tr35/tr35-numbers.html#Language_Plural_Rules)

## Mentioned Tools

- [Gettext](https://www.gnu.org/software/gettext/)
- [FormatJS](https://formatjs.io/)
- [React-intl](https://formatjs.io/docs/react-intl)
- [Angular ICU and XLIFF](https://angular.io/guide/i18n-common-translation-files)
- [i18next plural](https://www.i18next.com/translation-function/plurals)
- [Weblate](https://weblate.org/fr/)

- [extract-react-intl-messages](https://www.npmjs.com/package/extract-react-intl-messages)
- [react-intl-po](https://www.npmjs.com/package/react-intl-po)
- [react-intl-po-loader](https://www.npmjs.com/package/react-intl-po-loader)

## Rich Text Formatting

- [react-intl Rich Text Formatting](https://formatjs.io/docs/react-intl/components#rich-text-formatting)
- [projectfluent](https://projectfluent.org/)
